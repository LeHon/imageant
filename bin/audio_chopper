#!/bin/bash

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# The location of the main audio_chopper program, relative to this launch
# script.
AC_PATH="../src/audio_chopper_main.py"


# Get the location of this launch script.  If the script was not run from a
# symlink, the next two lines are all we need.
SRCPATH=${BASH_SOURCE[0]}
SRCDIR=$(dirname ${SRCPATH})

# If SRCPATH is a symlink, resolve the link (and any subsequent links) until we
# arrive at the actual script location.
while [ -L "${SRCPATH}" ]; do
    SRCPATH=$(readlink ${SRCPATH})

    # If the link target is a relative path, it is relative to the original
    # symlink location, so we must construct a new path for the link target
    # based on SRCDIR (the original symlink location).
    if [ "${SRCPATH:0:1}" != "/" ]; then
        SRCPATH="${SRCDIR}/${SRCPATH}"
    fi

    SRCDIR=$(dirname ${SRCPATH})
done

source "${SRCDIR}/../src/bash/py_launch.sh"

# Run audio_chopper, passing on all command-line arguments.
launch "${SRCDIR}/${AC_PATH}" "$@"

