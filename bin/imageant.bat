rem Copyright (C) 2020 Brian J. Stucky
rem 
rem This program is free software: you can redistribute it and/or modify
rem it under the terms of the GNU General Public License as published by
rem the Free Software Foundation, either version 3 of the License, or
rem (at your option) any later version.
rem 
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem 
rem You should have received a copy of the GNU General Public License
rem along with this program.  If not, see <http://www.gnu.org/licenses/>.

@echo off

setlocal enabledelayedexpansion

rem The location of the main ImageAnt program, relative to this launch script.
set IMAGEANTPATH=..\src\imageant_main.py

rem The location of Python 3.  If no location is set, we assume that "python"
rem or "python3" is in the user's PATH somewhere.
set PY_PATH=

rem Get the location of this launch script.  If the script was not run from a
rem symlink, the next two lines are all we need.
set SRCPATH=%~f0
set SRCDIR=%~dp0

rem If SRCPATH is a symlink, resolve the link (and any subsequent links) until
rem we arrive at the actual script location.
:while
dir "%SRCPATH%" | find "<SYMLINK>" >nul && (
 	for /f "tokens=2 delims=[]" %%i in ('dir "!SRCPATH!" ^| find "<SYMLINK>"') do set SRCPATH=%%i

	rem If the link target is a relative path, it is relative to the
	rem original symlink location, so we must construct a new path for the
	rem link target based on SRCDIR (the original symlink location).
 	if "!SRCPATH:~1,1!" neq ":" (
 		set SRCPATH=%SRCDIR%!SRCPATH!
 	)

 	for %%m in ("!SRCPATH!") do (
 		set SRCDIR=%%~dpm
 	)

 	goto :while
)

if not defined PY_PATH (
	set PY_PATH=py
)

rem Check if Python 3 is installed.
call :checkPython %PY_PATH%
if %ERRORLEVEL% neq 0 (
	set PY_PATH=python
	call :checkPython !PY_PATH!

	if !ERRORLEVEL! neq 0 (
		echo ERROR: Python 3 appears to be missing.
		echo Please install Python 3 in order to run ImageAnt.
		exit /B 1
	)
)

rem Run ImageAnt, passing on all command-line arguments.
%PY_PATH% "%SRCDIR%%IMAGEANTPATH%" %*

exit /B %ERRORLEVEL%


rem A "function" to check if a path string points to a Python 3 executable.
:checkPython
	rem Check if the path is valid.  From testing, it appears that this
	rem needs to be done as a step separate from version checking.
	"%~1" --version >nul 2>&1
	if %ERRORLEVEL% neq 0 (
		exit /B %ERRORLEVEL%
	)

	rem Check if it is a Python 3 executable. 
	"%~1" --version 2>&1 | findstr /B /C:"Python 3." >nul
	if %ERRORLEVEL% neq 0 (
		exit /B %ERRORLEVEL%
	)

	exit /B 0

endlocal
