# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
import unittest
from PyQt5.QtGui import QGuiApplication, QPixmap
import soundfile as sf
from imageant.annot_session import AnnotationSession, FILE_EXTS


class TestMediaFormatSupport(unittest.TestCase):
    def test_image_format_support(self):
        """
        Tests that all supported image formats are readable by QPixmap on the
        target OS.
        """
        exts = FILE_EXTS[AnnotationSession.MEDIA_IMAGES]

        if QGuiApplication.instance() is None:
            qapp = QGuiApplication([])

        for ext in exts:
            fpath = os.path.join('images/image_formats', 'gray' + ext)
            pm = QPixmap(fpath)
            self.assertFalse(pm.isNull())
            self.assertEqual(448, pm.width())
            self.assertEqual(448, pm.height())

    def test_audio_format_support(self):
        """
        Tests that all supported audio file formats are readable by SoundFile
        on the target OS.  Because audio data are played directly from memory
        after reading the data using SoundFile, this is also sufficient to test
        playback support.
        """
        exts = FILE_EXTS[AnnotationSession.MEDIA_AUDIO]

        for ext in exts:
            fpath = os.path.join('audio/audio_formats', 'A4_E5' + ext)
            au_data, samprate = sf.read(fpath)
            self.assertEqual(44100, samprate)
            self.assertEqual(2, au_data.shape[1])
            self.assertEqual(22050, au_data.shape[0])

