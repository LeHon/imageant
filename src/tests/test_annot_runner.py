# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import tempfile
import os.path
import shutil
import unittest
from imageant.annot_runner import AnnotationsRunner


class _ARObserverStub:
    """
    A simple class that acts as an observer of an AnnotationsRunner.
    """
    def __init__(self, runner):
        self.task_undo_cnt = 0
        self.task_undo_lastval = None

        self.img_undo_cnt = 0
        self.img_undo_lastval = None

        self.task_change_cnt = 0
        self.task_change_lastval = None

        self.img_change_cnt = 0
        self.img_change_lastval = None

        self.annot_state_cnt = 0
        self.annot_state_lastval = None

        self.annot_complete_cnt = 0

        runner.taskUndoStateChange.registerObserver(self.taskUndo)
        runner.mediaObjUndoStateChange.registerObserver(self.imageUndo)
        runner.annotationTaskChange.registerObserver(self.taskChange)
        runner.mediaObjectChange.registerObserver(self.imgChange)
        runner.annotationStateChange.registerObserver(self.annotState)
        runner.annotationsCompleted.registerObserver(self.annotComplete)

    def taskUndo(self, val):
        self.task_undo_cnt += 1
        self.task_undo_lastval = val

    def imageUndo(self, val):
        self.img_undo_cnt += 1
        self.img_undo_lastval = val

    def taskChange(self, task):
        self.task_change_cnt += 1
        self.task_change_lastval = task

    def imgChange(self, image):
        self.img_change_cnt += 1
        self.img_change_lastval = image

    def annotState(self, val):
        self.annot_state_cnt += 1
        self.annot_state_lastval = val

    def annotComplete(self):
        self.annot_complete_cnt += 1


#
# Image file names: 
#    'black.png', 'dark_blue.png', 'dark_green.png', 'dark_red.png',
#    'gray.png', 'light_blue.png', 'light_green.png', 'light_red.png',
#    'plain_blue.png', 'plain_green.png', 'plain_red.png', 'white.png'
#

class TestAnnotationsRunner(unittest.TestCase):
    """
    Integration tests for AnnotationsRunner.
    """
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _checkObsTaskUndo(self, obs, cnt, val):
        self.assertEqual(cnt, obs.task_undo_cnt)
        self.assertEqual(val, obs.task_undo_lastval)

    def _checkObsImageUndo(self, obs, cnt, val):
        self.assertEqual(cnt, obs.img_undo_cnt)
        self.assertEqual(val, obs.img_undo_lastval)

    def _checkObsTaskChange(self, obs, cnt, task):
        self.assertEqual(cnt, obs.task_change_cnt)

        if task is None:
            self.assertEqual(task, obs.task_change_lastval)
        else:
            self.assertEqual(task, obs.task_change_lastval.varname)

    def _checkObsImageChange(self, obs, cnt, image):
        if image is None:
            self.assertEqual(image, obs.img_change_lastval)
        else:
            self.assertEqual(image, os.path.basename(obs.img_change_lastval))

    def _checkObsAnnotState(self, obs, cnt, val):
        self.assertEqual(cnt, obs.annot_state_cnt)
        self.assertEqual(val, obs.annot_state_lastval)

    def _checkEmpty(self, ar, obs):
        """
        Checks the state of an "empty" AnnotationsRunner.
        """
        self.assertIsNone(ar.getScriptFile())
        self.assertIsNone(ar.getActiveMediaObject())
        self.assertIsNone(ar.getActiveTask())

        with self.assertRaisesRegex(Exception, 'No media objects are loaded.'):
            ar.getMediaObjCount()

        with self.assertRaisesRegex(Exception, 'No media objects are loaded.'):
            ar.getAnnotatedMediaObjCount()

    def _configureAnnotationsRunner(self, ar, restore=False):
        """
        Configures an AnnotationsRunner and checks the state.
        """
        outfile = os.path.join(self.tmpdir, 'test-out.csv')
        logfile = os.path.join(self.tmpdir, 'test-out.csv-log')

        ar.initAnnotations(
            'scripts/test_script.ias', 'images/12_images', outfile, logfile,
            restore
        )

        self.assertEqual('scripts/test_script.ias', ar.getScriptFile())
        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertTrue(ar.getMediaObjCount() > 0)
        self.assertEqual(
            'black.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(12, ar.getMediaObjCount())
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())

    def test_reset(self):
        """
        Integration tests that cover setting annotation actions and resetting
        an AnnotationsRunner.
        """
        ar = AnnotationsRunner()
        obs = _ARObserverStub(ar)
        self._configureAnnotationsRunner(ar)

        self._checkObsTaskUndo(obs, 0, None)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 1, 'flowers')
        self._checkObsImageChange(obs, 1, 'black.png')
        self._checkObsAnnotState(obs, 1, True)
        
        # Fully annotate the first image.
        ar.setResponse(0, 0.23)
        ar.setResponse(0, 1.0)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(1.0, ar._am.getMediaFileTime('black.png'))
        self._checkObsTaskUndo(obs, 1, True)
        self._checkObsImageUndo(obs, 1, True)
        self._checkObsTaskChange(obs, 3, 'flowers')
        self._checkObsImageChange(obs, 2, 'dark_blue.png')
        self._checkObsAnnotState(obs, 1, True)

        # Fully reset the AnnotationsRunner.
        ar.reset()
        self._checkEmpty(ar, obs)
        self._checkObsTaskUndo(obs, 2, False)
        self._checkObsImageUndo(obs, 2, False)
        self._checkObsTaskChange(obs, 4, None)
        self._checkObsImageChange(obs, 3, None)
        self._checkObsAnnotState(obs, 2, False)

        self._configureAnnotationsRunner(ar)

        # Fully annotate the first image and complete the first task for the
        # second image.
        ar.setResponse(0, 0.24)
        ar.setResponse(0, 1.14)
        ar.setResponse(0, 2.0)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(1.14, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(0.0, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 3, True)
        self._checkObsImageUndo(obs, 3, True)
        self._checkObsTaskChange(obs, 8, 'whole_plant')
        self._checkObsImageChange(obs, 4, 'dark_blue.png')
        self._checkObsAnnotState(obs, 3, True)

        # Reset the script, image folder, and output files atomically.
        outfile = os.path.join(self.tmpdir, 'test-out.csv')
        logfile = os.path.join(self.tmpdir, 'test-out.csv-log')
        ar.initAnnotations(
            'scripts/test_script.ias', 'images/12_images', outfile, logfile,
            False
        )

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual(12, ar.getMediaObjCount())
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self.assertEqual(0.0, ar._am.getMediaFileTime('black.png'))
        self._checkObsTaskUndo(obs, 4, False)
        self._checkObsImageUndo(obs, 4, False)
        self._checkObsTaskChange(obs, 9, 'flowers')
        self._checkObsImageChange(obs, 5, 'black.png')
        self._checkObsAnnotState(obs, 3, True)

        self.assertEqual(0, obs.annot_complete_cnt)

    def test_restore(self):
        """
        Integration tests that involve setting annotation actions and restoring
        annotations state from a log file.
        """
        ar = AnnotationsRunner()
        obs = _ARObserverStub(ar)
        self._configureAnnotationsRunner(ar)

        self._checkObsTaskUndo(obs, 0, None)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 1, 'flowers')
        self._checkObsImageChange(obs, 1, 'black.png')
        self._checkObsAnnotState(obs, 1, True)
        
        # Fully annotate the first image and partially annotate the second
        # image.
        ar.setResponse(0, 0.24)
        ar.setResponse(0, 1.14)
        ar.setResponse(0, 2.0)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(1.14, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(0.0, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 1, True)
        self._checkObsImageUndo(obs, 1, True)
        self._checkObsTaskChange(obs, 4, 'whole_plant')
        self._checkObsImageChange(obs, 2, 'dark_blue.png')
        self._checkObsAnnotState(obs, 1, True)

        # Reload the script, reload the image folder, and restore state as a
        # single, atomic operation.  This should not trigger any image state or
        # annotation state changes.
        outfile = os.path.join(self.tmpdir, 'test-out.csv')
        logfile = os.path.join(self.tmpdir, 'test-out.csv-log')
        ar.initAnnotations(
            'scripts/test_script.ias', 'images/12_images', outfile, logfile,
            True
        )

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(1.14, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(0.0, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 2, False)
        self._checkObsImageUndo(obs, 2, False)
        self._checkObsTaskChange(obs, 5, 'flowers')
        self._checkObsImageChange(obs, 2, 'dark_blue.png')
        self._checkObsAnnotState(obs, 1, True)

        # Partially annotate the second image.
        ar.setResponse(0, 2.0)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(1.14, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(0.0, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 3, True)
        self._checkObsImageUndo(obs, 2, False)
        self._checkObsTaskChange(obs, 6, 'whole_plant')
        self._checkObsImageChange(obs, 2, 'dark_blue.png')
        self._checkObsAnnotState(obs, 1, True)

    def test_workflows(self):
        """
        Integration tests that cover various annotation workflow paths,
        including many undo states and undo operations.
        """
        ar = AnnotationsRunner()
        obs = _ARObserverStub(ar)
        self._checkEmpty(ar, obs)
        self._checkObsTaskUndo(obs, 0, None)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 0, None)
        self._checkObsImageChange(obs, 0, None)
        self._checkObsAnnotState(obs, 0, None)

        self._configureAnnotationsRunner(ar)

        self.assertEqual(12, ar.getMediaObjCount())
        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self._checkObsTaskChange(obs, 1, 'flowers')
        self._checkObsImageChange(obs, 1, 'black.png')
        self._checkObsAnnotState(obs, 1, True)

        self.assertEqual(0, obs.annot_complete_cnt)

        # Verify that if we attempt to undo with no undo information, there is
        # no state change.
        ar.undoLastTask(0.0)
        ar.undoLastMediaObject(0.0)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self._checkObsTaskUndo(obs, 0, None)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 1, 'flowers')
        self._checkObsImageChange(obs, 1, 'black.png')
        self._checkObsAnnotState(obs, 1, True)

        # Do one annotation task.
        ar.setResponse(0, 0.24)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self.assertEqual(0.0, ar._am.getMediaFileTime('black.png'))
        self._checkObsTaskUndo(obs, 1, True)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 2, 'whole_plant')
        self._checkObsImageChange(obs, 1, 'black.png')

        # Undo the annotation.
        ar.undoLastTask(0.5)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self._checkObsTaskUndo(obs, 2, False)
        self._checkObsImageUndo(obs, 0, None)
        self._checkObsTaskChange(obs, 3, 'flowers')
        self._checkObsImageChange(obs, 1, 'black.png')

        # Fully annotate the first image.
        ar.setResponse(0, 1.0)
        ar.setResponse(0, 2.4)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(2.4, ar._am.getMediaFileTime('black.png'))
        self._checkObsTaskUndo(obs, 3, True)
        self._checkObsImageUndo(obs, 1, True)
        self._checkObsTaskChange(obs, 5, 'flowers')
        self._checkObsImageChange(obs, 2, 'dark_blue.png')

        # Undo the annotation.
        ar.undoLastTask(1.12)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self.assertEqual(2.4, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(1.12, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 3, True)
        self._checkObsImageUndo(obs, 2, False)
        self._checkObsTaskChange(obs, 6, 'whole_plant')
        self._checkObsImageChange(obs, 3, 'black.png')

        # Fully annotate the first image.
        ar.setResponse(0, 1.0)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual(
            'dark_blue.png', os.path.basename(ar.getActiveMediaObject())
        )
        self.assertEqual(1, ar.getAnnotatedMediaObjCount())
        self.assertEqual(3.4, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(1.12, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 3, True)
        self._checkObsImageUndo(obs, 3, True)
        self._checkObsTaskChange(obs, 7, 'flowers')
        self._checkObsImageChange(obs, 4, 'dark_blue.png')

        # Undo the previous image.
        ar.undoLastMediaObject(1.0)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual('black.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(0, ar.getAnnotatedMediaObjCount())
        self.assertEqual(3.4, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(2.12, ar._am.getMediaFileTime('dark_blue.png'))
        self._checkObsTaskUndo(obs, 4, False)
        self._checkObsImageUndo(obs, 4, False)
        self._checkObsTaskChange(obs, 7, 'flowers')
        self._checkObsImageChange(obs, 5, 'black.png')

        # Annotate all images.
        self._checkObsAnnotState(obs, 1, True)
        self.assertEqual(0, obs.annot_complete_cnt)

        for cnt in range(12):
            ar.setResponse(0, 1.0 * cnt)
            ar.setResponse(0, 1.0 * cnt)

        self.assertIsNone(ar.getActiveTask())
        self.assertIsNone(ar.getActiveMediaObject())
        self.assertEqual(12, ar.getAnnotatedMediaObjCount())

        self.assertEqual(3.4, ar._am.getMediaFileTime('black.png'))
        self.assertEqual(3.12, ar._am.getMediaFileTime('dark_blue.png'))
        for cnt, fname in enumerate([
            'dark_green.png', 'dark_red.png', 'gray.png', 'light_blue.png',
            'light_green.png', 'light_red.png', 'plain_blue.png',
            'plain_green.png', 'plain_red.png', 'white.png'
        ], start=2):
            self.assertEqual(cnt * 1.0, ar._am.getMediaFileTime(fname))

        self._checkObsTaskUndo(obs, 5, True)
        self._checkObsImageUndo(obs, 5, True)
        self._checkObsTaskChange(obs, 31, None)
        self._checkObsImageChange(obs, 17, None)
        self._checkObsAnnotState(obs, 2, False)
        self.assertEqual(1, obs.annot_complete_cnt)
        
        # Undo the last annotation task.
        ar.undoLastTask(0.0)

        self.assertEqual('whole_plant', ar.getActiveTask().varname)
        self.assertEqual('white.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(11, ar.getAnnotatedMediaObjCount())
        self.assertEqual(11.0, ar._am.getMediaFileTime('white.png'))
        self._checkObsTaskUndo(obs, 5, True)
        self._checkObsImageUndo(obs, 5, True)
        self._checkObsTaskChange(obs, 32, 'whole_plant')
        self._checkObsImageChange(obs, 18, 'white.png')
        self._checkObsAnnotState(obs, 3, True)
        self.assertEqual(1, obs.annot_complete_cnt)

        # Annotate all images.
        ar.setResponse(0, 1.0)

        self.assertIsNone(ar.getActiveTask())
        self.assertIsNone(ar.getActiveMediaObject())
        self.assertEqual(12, ar.getAnnotatedMediaObjCount())
        self.assertEqual(12.0, ar._am.getMediaFileTime('white.png'))
        self._checkObsTaskUndo(obs, 5, True)
        self._checkObsImageUndo(obs, 5, True)
        self._checkObsTaskChange(obs, 33, None)
        self._checkObsImageChange(obs, 19, None)
        self._checkObsAnnotState(obs, 4, False)
        self.assertEqual(2, obs.annot_complete_cnt)

        # Undo the last image.
        ar.undoLastMediaObject(0.0)

        self.assertEqual('flowers', ar.getActiveTask().varname)
        self.assertEqual('white.png', os.path.basename(ar.getActiveMediaObject()))
        self.assertEqual(11, ar.getAnnotatedMediaObjCount())
        self.assertEqual(12.0, ar._am.getMediaFileTime('white.png'))
        self._checkObsTaskUndo(obs, 5, True)
        self._checkObsImageUndo(obs, 5, True)
        self._checkObsTaskChange(obs, 34, 'flowers')
        self._checkObsImageChange(obs, 20, 'white.png')
        self._checkObsAnnotState(obs, 5, True)
        self.assertEqual(2, obs.annot_complete_cnt)

