# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from imageant.bidirectional_list import BidirectionalList as bilist


class TestBidirectionalList(unittest.TestCase):
    def test_append_and_lookup(self):
        bl = bilist()
        self.assertEqual(0, len(bl))

        bl.append('a')
        self.assertEqual(1, len(bl))
        self.assertEqual('a', bl[0])
        self.assertEqual(0, bl.r('a'))

        bl.append('b')
        self.assertEqual(2, len(bl))
        self.assertEqual('a', bl[0])
        self.assertEqual(0, bl.r('a'))
        self.assertEqual('b', bl[1])
        self.assertEqual(1, bl.r('b'))

        bl.append('c')
        self.assertEqual(3, len(bl))
        self.assertEqual('a', bl[0])
        self.assertEqual(0, bl.r('a'))
        self.assertEqual('b', bl[1])
        self.assertEqual(1, bl.r('b'))
        self.assertEqual('c', bl[2])
        self.assertEqual(2, bl.r('c'))

        with self.assertRaisesRegex(ValueError, 'must be unique'):
            bl.append('b')

    def test_init(self):
        exp = ['a', 'b', 'c']
        bl = bilist(exp)
        self.assertEqual(3, len(bl))

        for cnt, value in enumerate(exp):
            self.assertEqual(value, bl[cnt])
            self.assertEqual(cnt, bl.r(value))

    def test_iterate(self):
        bl = bilist(['a', 'b', 'c'])
        self.assertEqual(3, len(bl))

        cnt = 0
        for value in bl:
            self.assertEqual(value, bl[cnt])
            self.assertEqual(cnt, bl.r(value))
            cnt += 1

        self.assertEqual(3, cnt)

    def test_sort(self):
        bl = bilist(['d', 'c', 'a', 'b'])
        bl.sort()
        self.assertEqual(4, len(bl))

        exp = ['a', 'b', 'c', 'd']
        for cnt, value in enumerate(exp):
            self.assertEqual(value, bl[cnt])
            self.assertEqual(cnt, bl.r(value))

        bl.sort(reverse=True)

        exp = ['d', 'c', 'b', 'a']
        for cnt, value in enumerate(exp):
            self.assertEqual(value, bl[cnt])
            self.assertEqual(cnt, bl.r(value))

    def test_contains(self):
        bl = bilist(['a', 'b', 'c'])

        self.assertTrue('a' in bl)
        self.assertTrue('b' in bl)
        self.assertTrue('c' in bl)
        self.assertFalse('d' in bl)
        self.assertFalse(0 in bl)

