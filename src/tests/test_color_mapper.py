# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
import unittest
from PyQt5.QtGui import QGuiApplication, QPixmap
import numpy as np
from imageant.audio.color_mapper import ColorMapper


class TestColorMapper(unittest.TestCase):
    def setUp(self):
        self.cm = ColorMapper()
        self.cm.setMapping('audacity')

    def test_buildGrayscaleArray(self):
        vals = self.cm._buildGrayscaleArray(6)

        self.assertEqual((6,3), vals.shape)

        expvals = (255, 204, 153, 102, 51, 0)
        for channel in range(3):
            self.assertTrue(np.array_equal(expvals, vals[:,channel]))

    def test_buildGradientArray(self):
        vals = self.cm._buildGradientArray(10)

        self.assertEqual((9,3), vals.shape)

        expvals_c0 = (191, 134, 76, 153, 230, 242, 255, 255, 255)
        expvals_c1 = (191, 172, 153, 89, 26, 13, 0, 128, 255)
        expvals_c2 = (191, 223, 255, 242, 230, 115, 0, 128, 255)
        self.assertTrue(np.array_equal(expvals_c0, vals[:,0]))
        self.assertTrue(np.array_equal(expvals_c1, vals[:,1]))

    def test_mapToColorsMulti(self):
        spec = np.array([
            [0.0, 0.5, 1.0],
            [0.2, 0.6, 0.8]
        ])

        # Expected colors for a gradient lookup table of size ~10 using the
        # 'audacity' color gradient:
        # [[191 191 191]
        #  [134 172 223]
        #  [ 76 153 255]
        #  [153  89 242]
        #  [230  26 230]
        #  [242  13 115]
        #  [255   0   0]
        #  [255 128 128]
        #  [255 255 255]]

        # Spectrogram image with no padding.
        expvals = np.array([
            # Row 1 of the spectrogram, top row of image pixels.
            [76, 153, 255],
            [242, 13, 115],
            [255, 0, 0],
            # Row 0 of the spectrogram, bottom row of image pixels.
            [191, 191, 191],
            [230, 26, 230],
            [255, 255, 255]
        ])
        ivals = self.cm.mapToColorsMulti(spec, 0, 0, 10)
        self.assertTrue(np.array_equal(expvals, ivals))

        # Spectrogram image with 1 pixel of padding on the left and 2 pixels of
        # padding on the right.
        expvals = np.array([
            # Row 1 of the spectrogram, top row of image pixels.
            [191, 191, 191],
            [76, 153, 255],
            [242, 13, 115],
            [255, 0, 0],
            [191, 191, 191],
            [191, 191, 191],
            # Row 0 of the spectrogram, bottom row of image pixels.
            [191, 191, 191],
            [191, 191, 191],
            [230, 26, 230],
            [255, 255, 255],
            [191, 191, 191],
            [191, 191, 191]
        ])
        ivals = self.cm.mapToColorsMulti(spec, 1, 2, 10)
        self.assertTrue(np.array_equal(expvals, ivals))

