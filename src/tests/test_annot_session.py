# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import tempfile
import os.path
import shutil
import unittest
from imageant.annot_session import AnnotationSession


class TestAnnotationSession(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.sess_path = os.path.join(self.tmpdir, 'test_sess.ia_sess')

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _createImageSession(self):
        sess = AnnotationSession()

        sess.updateSession(
            self.sess_path, 'scripts/test_script.ias', 'images/12_images',
            AnnotationSession.MEDIA_IMAGES
        )

        return sess

    def _createAudioSession(self):
        sess = AnnotationSession()

        sess.updateSession(
            self.sess_path, 'scripts/test_script.ias', 'audio',
            AnnotationSession.MEDIA_AUDIO
        )

        return sess

    def _checkImageSessionProperties(self, sess):
        script_path = os.path.abspath('scripts/test_script.ias')

        self.assertEqual(script_path, sess.getScriptFile())
        self.assertEqual(
            os.path.abspath('images/12_images'), sess.getMediaFolder()
        )
        self.assertEqual(AnnotationSession.MEDIA_IMAGES, sess.getMediaType())
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.csv'), sess.getOutputFile()
        )
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.log'), sess.getLogFile()
        )

        runner = sess.getRunner()
        self.assertEqual(script_path, runner.getScriptFile())
        self.assertEqual(12, runner.getMediaObjCount())
        self.assertEqual(
            'black.png', os.path.basename(runner.getActiveMediaObject())
        )
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.csv'), runner.getOutputFile()
        )

    def _checkAudioSessionProperties(self, sess):
        script_path = os.path.abspath('scripts/test_script.ias')

        self.assertEqual(
            os.path.abspath('scripts/test_script.ias'), sess.getScriptFile()
        )
        self.assertEqual(
            os.path.abspath('audio'), sess.getMediaFolder()
        )
        self.assertEqual(AnnotationSession.MEDIA_AUDIO, sess.getMediaType())
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.csv'), sess.getOutputFile()
        )
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.log'), sess.getLogFile()
        )

        runner = sess.getRunner()
        self.assertEqual(script_path, runner.getScriptFile())
        self.assertEqual(3, runner.getMediaObjCount())
        self.assertEqual(
            'choppedaudio_long.wav',
            os.path.basename(runner.getActiveMediaObject())
        )
        self.assertEqual(
            os.path.join(self.tmpdir, 'test_sess.csv'), runner.getOutputFile()
        )

    def test_updateSession(self):
        sess = self._createImageSession()
        self._checkImageSessionProperties(sess)

        sess = self._createAudioSession()
        self._checkAudioSessionProperties(sess)

        with self.assertRaisesRegex(ValueError, 'Invalid media type'):
            sess.updateSession(
                self.sess_path, 'scripts/test_script.ias', 'images/12_images',
                -1
            )

    def test_restoreSession(self):
        # Test creating and restoring an image session.
        sess = self._createImageSession()

        new_sess = AnnotationSession()
        new_sess.restoreSession(sess.getSessionFile())

        self._checkImageSessionProperties(new_sess)

        # Test creating and restoring an audio session.
        sess = self._createAudioSession()

        new_sess = AnnotationSession()
        new_sess.restoreSession(sess.getSessionFile())

        self._checkAudioSessionProperties(new_sess)

