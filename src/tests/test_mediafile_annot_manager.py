# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import tempfile
import os.path
import shutil
import unittest
from imageant.mediafile_annot_manager import MediaFileAnnotationsManager


# Test image file names and annotations.
imgfiles = [
    'black.png', 'dark_blue.png', 'dark_green.png', 'dark_red.png', 'gray.png',
    'light_blue.png', 'light_green.png', 'light_red.png', 'plain_blue.png',
    'plain_green.png', 'plain_red.png', 'white.png'
]

ann_vals = {
    'black.png': {'var1': 't', 'var2': '1'},
    'dark_blue.png': {'var1': 't', 'var2': '2'},
    'dark_green.png': {'var1': 't', 'var2': '3'},
    'dark_red.png': {'var1': 't', 'var2': '4'},
    'gray.png': {'var1': 't', 'var2': '5'},
    'light_blue.png': {'var1': 't', 'var2': '6'},
    'light_green.png': {'var1': 'f', 'var2': '7'},
    'light_red.png': {'var1': 'f', 'var2': '8'},
    'plain_blue.png': {'var1': 'f', 'var2': '9'},
    'plain_green.png': {'var1': 'f', 'var2': '10'},
    'plain_red.png': {'var1': 'f', 'var2': '11'},
    'white.png': {'var1': 'f', 'var2': '12'}
}


class TestMediaFileAnnotationsManager(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        #pass
        shutil.rmtree(self.tmpdir)

    def _getAnnotationsManager(self, imgs_dir, restore=False, file_exts=None):
        dirname = os.path.basename(os.path.normpath(imgs_dir))

        outfile = os.path.join(self.tmpdir, dirname + '-out.csv')
        logfile = os.path.join(self.tmpdir, dirname + '~out.csv-log')

        am = MediaFileAnnotationsManager(
            imgs_dir, outfile, logfile, restore, file_exts
        )

        return am

    def test_init(self):
        # Check loading all image files with no file extension filtering.
        am = self._getAnnotationsManager('images/12_images')
        self.assertEqual('black.png', am.getActiveMediaFile())
        self.assertEqual(12, am.getMediaFileCount())
        self.assertEqual(0, am.getAnnotatedMediaFileCount())
        self.assertSequenceEqual(imgfiles, am.getMediaFileNames())

        for img_fname in imgfiles:
            self.assertEqual(0.0, am.getMediaFileTime(img_fname))

        with self.assertRaisesRegex(ValueError, 'not part of .* file set'):
            am.getMediaFileTime('invalid.png')

        am.close()

        # Check loading all image files with ".jpg",".png" extension filtering.
        am = self._getAnnotationsManager(
            'images/12_images', False, ['.jpg', '.png']
        )
        self.assertEqual('black.png', am.getActiveMediaFile())
        self.assertEqual(12, am.getMediaFileCount())
        self.assertEqual(0, am.getAnnotatedMediaFileCount())
        self.assertSequenceEqual(imgfiles, am.getMediaFileNames())

        # Check loading all image files with ".jpg" extension filtering.
        with self.assertRaisesRegex(Exception, 'No media files were found'):
            am = self._getAnnotationsManager(
                'images/12_images', False, ['.jpg']
            )

        am.close()

    def test_setAnnotations(self):
        am = self._getAnnotationsManager('images/12_images')

        am.setAnnotations(ann_vals[imgfiles[0]], 1.23)
        self.assertEqual(imgfiles[1], am.getActiveMediaFile())
        self.assertEqual(1, am.getAnnotatedMediaFileCount())
        self.assertEqual(1.23, am.getMediaFileTime(imgfiles[0]))

        with self.assertRaisesRegex(
            Exception,
            'variable names .* do not match .* previous annotations.'
        ):
            am.setAnnotations({'var1': 't', 'var3': 'f'}, 1.23)

        self.assertEqual(1, am.getAnnotatedMediaFileCount())

        for cnt, img_file in enumerate(imgfiles[1:-1], start=1):
            am.setAnnotations(ann_vals[img_file], cnt + 1.23)
            self.assertEqual(imgfiles[cnt + 1], am.getActiveMediaFile())
            self.assertEqual(cnt + 1, am.getAnnotatedMediaFileCount())
            self.assertEqual(cnt + 1.23, am.getMediaFileTime(img_file))

        am.setAnnotations(ann_vals[imgfiles[-1]], 0.123)
        self.assertEqual(None, am.getActiveMediaFile())
        self.assertEqual(12, am.getAnnotatedMediaFileCount())
        self.assertEqual(0.123, am.getMediaFileTime(imgfiles[-1]))

        self.assertEqual(ann_vals, am.getAnnotations())

        am.close()

    def test_workflow(self):
        """
        Integration test that runs through multiple annotations scenarios.
        """
        with self._getAnnotationsManager('images/1_image') as am:
            # Test annotating a single-image set.
            am.setAnnotations(ann_vals['gray.png'], 1.23)
            self.assertEqual(None, am.getActiveMediaFile())
            self.assertEqual(1, am.getAnnotatedMediaFileCount())
            self.assertEqual(1.23, am.getMediaFileTime('gray.png'))

            with self.assertRaisesRegex(
                Exception, 'there is no active.* media'
            ):
                am.skipActiveMediaFile(2.4)

            with self.assertRaisesRegex(
                Exception, 'there is no active.* media'
            ):
                am.setAnnotations(ann_vals[imgfiles[0]], 2.4)

        with self._getAnnotationsManager('images/1_image') as am:
            # Test skipping on a single-image set.
            am.skipActiveMediaFile(0.23)
            self.assertEqual('gray.png', am.getActiveMediaFile())
            self.assertEqual(0, am.getAnnotatedMediaFileCount())
            self.assertEqual(0.23, am.getMediaFileTime('gray.png'))

            am.setAnnotations(ann_vals[imgfiles[0]], 1.0)
            self.assertEqual(None, am.getActiveMediaFile())
            self.assertEqual(1, am.getAnnotatedMediaFileCount())
            self.assertEqual(1.23, am.getMediaFileTime('gray.png'))

        am = self._getAnnotationsManager('images/12_images')

        # Test image skipping on a larger image set.
        # Skip image 0.
        am.skipActiveMediaFile(0.23)
        self.assertEqual(imgfiles[1], am.getActiveMediaFile())
        self.assertEqual(0, am.getAnnotatedMediaFileCount())
        self.assertEqual(0.23, am.getMediaFileTime(imgfiles[0]))
        am.setAnnotations(ann_vals[imgfiles[1]], 2.34)
        am.setAnnotations(ann_vals[imgfiles[2]], 3.34)
        am.setAnnotations(ann_vals[imgfiles[3]], 4.34)
        self.assertEqual(3, am.getAnnotatedMediaFileCount())
        # Skip image 4.
        am.skipActiveMediaFile(10.01)
        self.assertEqual(imgfiles[5], am.getActiveMediaFile())
        self.assertEqual(3, am.getAnnotatedMediaFileCount())
        self.assertEqual(10.01, am.getMediaFileTime(imgfiles[4]))
        am.setAnnotations(ann_vals[imgfiles[5]], 5.34)
        am.setAnnotations(ann_vals[imgfiles[6]], 6.34)
        am.setAnnotations(ann_vals[imgfiles[7]], 7.34)
        am.setAnnotations(ann_vals[imgfiles[8]], 8.34)
        am.setAnnotations(ann_vals[imgfiles[9]], 9.34)
        am.setAnnotations(ann_vals[imgfiles[10]], 10.34)
        am.setAnnotations(ann_vals[imgfiles[11]], 11.34)
        self.assertEqual(imgfiles[0], am.getActiveMediaFile())
        self.assertEqual(10, am.getAnnotatedMediaFileCount())
        # Skip image 0 again.
        am.skipActiveMediaFile(1.0)
        self.assertEqual(imgfiles[4], am.getActiveMediaFile())
        self.assertEqual(10, am.getAnnotatedMediaFileCount())
        self.assertEqual(1.23, am.getMediaFileTime(imgfiles[0]))
        # Annotate image 4.
        am.setAnnotations(ann_vals[imgfiles[4]], 10.01)
        self.assertEqual(imgfiles[0], am.getActiveMediaFile())
        self.assertEqual(11, am.getAnnotatedMediaFileCount())
        self.assertEqual(20.02, am.getMediaFileTime(imgfiles[4]))
        # Annotate image 0.
        am.setAnnotations(ann_vals[imgfiles[0]], 2.0)
        self.assertEqual(None, am.getActiveMediaFile())
        self.assertEqual(12, am.getAnnotatedMediaFileCount())
        self.assertEqual(3.23, am.getMediaFileTime(imgfiles[0]))

        # Test resetting an image when all annotations are complete (i.e., the
        # current image is None).
        am.resetMediaFile(imgfiles[6], 1.0)
        self.assertEqual(imgfiles[6], am.getActiveMediaFile())
        self.assertEqual(11, am.getAnnotatedMediaFileCount())
        self.assertEqual(6.34, am.getMediaFileTime(imgfiles[6]))
        am.skipActiveMediaFile(1.3)
        self.assertEqual(imgfiles[6], am.getActiveMediaFile())
        self.assertEqual(11, am.getAnnotatedMediaFileCount())
        self.assertEqual(7.64, am.getMediaFileTime(imgfiles[6]))

        # Test resetting an image when there is already an active image.
        am.resetMediaFile(imgfiles[8], 2.0)
        self.assertEqual(imgfiles[8], am.getActiveMediaFile())
        self.assertEqual(10, am.getAnnotatedMediaFileCount())
        self.assertEqual(9.64, am.getMediaFileTime(imgfiles[6]))
        self.assertEqual(8.34, am.getMediaFileTime(imgfiles[8]))

        # Annotate image 8.
        am.setAnnotations(ann_vals[imgfiles[8]], 1.0)
        self.assertEqual(imgfiles[6], am.getActiveMediaFile())
        self.assertEqual(11, am.getAnnotatedMediaFileCount())
        self.assertEqual(9.34, am.getMediaFileTime(imgfiles[8]))
        # Annotate image 6.
        am.setAnnotations(ann_vals[imgfiles[6]], 2.0)
        self.assertEqual(None, am.getActiveMediaFile())
        self.assertEqual(12, am.getAnnotatedMediaFileCount())
        self.assertEqual(11.64, am.getMediaFileTime(imgfiles[6]))

        am.close()

    def _compareRestored(self, am):
        """
        Accepts an AnnotationsManager object, restores the logged state into a
        newAnnotationsManager object, and compares the annotations state and
        the current media file.  Also verifies that no actions are logged when
        restoring state from a log file.
        """
        init_size = os.path.getsize(am.getLogFile())

        with self._getAnnotationsManager(am.getMediaFolder(), restore=True) as am2:
            self.assertEqual(am.getActiveMediaFile(), am2.getActiveMediaFile())
            self.assertEqual(
                am.getAnnotatedMediaFileCount(),
                am2.getAnnotatedMediaFileCount()
            )
            self.assertEqual(am.getMediaFileCount(), am2.getMediaFileCount())
            self.assertEqual(am.getAnnotations(), am2.getAnnotations())

            for imgfile in am.getMediaFileNames():
                self.assertEqual(
                    am.getMediaFileTime(imgfile), am2.getMediaFileTime(imgfile)
                )

            self.assertEqual(init_size, os.path.getsize(am2.getLogFile()))

    def test_logging(self):
        # Test annotating a single-image set.
        with self._getAnnotationsManager('images/1_image') as am:
            am.setAnnotations(ann_vals['gray.png'], 1.23)
            self._compareRestored(am)

        # Test skipping on a single-image set.
        with self._getAnnotationsManager('images/1_image') as am:
            am = self._getAnnotationsManager('images/1_image')
            am.skipActiveMediaFile(0.23)
            am.skipActiveMediaFile(1.0)
            am.setAnnotations(ann_vals['gray.png'], 2.0)
            self.assertEqual(3.23, am.getMediaFileTime('gray.png'))
            self._compareRestored(am)

        # Test image skipping and resetting on a larger image set.
        am = self._getAnnotationsManager('images/12_images')
        # Skip image 0.
        am.skipActiveMediaFile(0.23)
        am.setAnnotations(ann_vals[imgfiles[1]], 1.24)
        am.setAnnotations(ann_vals[imgfiles[2]], 2.24)
        am.setAnnotations(ann_vals[imgfiles[3]], 3.24)
        # Skip image 4.
        am.skipActiveMediaFile(10.02)
        am.setAnnotations(ann_vals[imgfiles[5]], 5.24)
        am.setAnnotations(ann_vals[imgfiles[6]], 6.24)

        # Test logging for an incomplete set of annotations.
        self._compareRestored(am)

        # Continue annotating.
        am.setAnnotations(ann_vals[imgfiles[7]], 7.24)
        am.setAnnotations(ann_vals[imgfiles[8]], 8.24)
        am.setAnnotations(ann_vals[imgfiles[9]], 9.24)
        am.setAnnotations(ann_vals[imgfiles[10]], 10.24)
        am.setAnnotations(ann_vals[imgfiles[11]], 11.24)

        # Test logging for an incomplete set of annotations.
        self._compareRestored(am)

        # Continue annotating; skip image 0 again.
        am.skipActiveMediaFile(2.0)
        am.setAnnotations(ann_vals[imgfiles[4]], 3.0)
        am.setAnnotations(ann_vals[imgfiles[0]], 4.0)
        self.assertEqual(6.23, am.getMediaFileTime(imgfiles[0]))
        self.assertEqual(13.02, am.getMediaFileTime(imgfiles[4]))

        # Test logging for the complete set of annotations.
        self._compareRestored(am)

        # Test resetting an image and changing its annotation.
        am.resetMediaFile(imgfiles[6], 0.0)
        am.skipActiveMediaFile(2.0)
        # Reset another image.
        am.resetMediaFile(imgfiles[8], 12.0)
        am.setAnnotations(ann_vals[imgfiles[0]], 4.0)
        am.setAnnotations(ann_vals[imgfiles[0]], 20.0)
        self.assertEqual(40.24, am.getMediaFileTime(imgfiles[6]))
        self.assertEqual(12.24, am.getMediaFileTime(imgfiles[8]))

        # Test logging for the complete set of annotations.
        self._compareRestored(am)

        am.close()

