# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Contains utility classes for working with annotations generated by ImageAnt
# (e.g., for generating consensus annnotations or other summary statistics).

from operator import itemgetter


class AnnotationStats:
    """
    Implements automatic counting of annotation values and summary statistic
    calculations for a single annotation question.
    """
    def __init__(self):
        self.vals = {}

    def update(self, annot_val):
        if annot_val not in self.vals:
            self.vals[annot_val] = 0

        self.vals[annot_val] += 1
    
    def __getitem__(self, key):
        return self.vals[key]

    def __str__(self):
        return str(self.vals)

    def __repr__(self):
        return str(self)

    def getAnnotVals(self):
        """
        Returns all unique annotation values.
        """
        return sorted(self.vals.keys())

    def getCountTotal(self):
        """
        Returns the total number of annotations.
        """
        return sum(self.vals.values())

    def getProportion(self, annot_val):
        """
        Returns the proportion of annotations with the given value.
        """
        if annot_val not in self.vals:
            raise KeyError(annot_val)

        return self.vals[annot_val] / self.getCountTotal()    

    def getMajority(self, none_val=None):
        for annot_val in self.vals:
            if self.getProportion(annot_val) > 0.5:
                return annot_val

        return none_val

    def getPlurality(self, none_val=None):
        # Create a list of count, value pairs.
        pairs = [(cnt, annot_val) for annot_val, cnt in self.vals.items()]

        # Sort with the largest counts at the beginning of the list.
        pairs.sort(key=itemgetter(0), reverse=True)

        if len(pairs) == 0:
            return none_val
        else:
            if len(pairs) > 1 and pairs[0][0] == pairs[1][0]:
                return none_val
            else:
                return pairs[0][1]


class MediaObjectAnnotationStats:
    """
    Keeps track of annotation statistics for one or more media objects.
    """
    def __init__(self, include_plurality=False, none_val=None):
        self.times = {}
        self.vals = {}
        self.varnames = set()
        self.include_plurality = include_plurality
        self.none_val = none_val

    def update(self, media_obj_id, annotations):
        """
        Adds a set of annotations for the given media object ID.  Annotations
        with a variable name of 'file' will be ignored.

        annotations: A dictionary of annotation values of the form
            {'variable_name': 'value', ...}.
        """
        for varname, val in annotations.items():
            if varname == 'file':
                continue

            if varname == 'time':
                if media_obj_id not in self.times:
                    self.times[media_obj_id] = []
                self.times[media_obj_id].append(float(val))
            else:
                if varname not in self.varnames:
                    self.varnames.add(varname)

                if media_obj_id not in self.vals:
                    self.vals[media_obj_id] = {}
                if varname not in self.vals[media_obj_id]:
                    self.vals[media_obj_id][varname] = AnnotationStats()

                self.vals[media_obj_id][varname].update(val)

    def getMediaObjIds(self):
        """
        Returns a list of all media object IDs encountered in calls to update().
        """
        return sorted(self.vals.keys())

    def getVarNames(self):
        """
        Returns a list of all variable names required for the annotation
        variables encountered in calls to update().
        """
        exp_varnames = []

        for varname in sorted(self.varnames):
            exp_varnames.append(varname + '-n')
            exp_varnames.append(varname + '-majority')
            if self.include_plurality:
                exp_varnames.append(varname + '-plurality')
            exp_varnames.append(varname + '-all')

        if len(self.times) > 0:
            exp_varnames.append('time-n')
            exp_varnames.append('time-mean')
            exp_varnames.append('time-min')
            exp_varnames.append('time-max')
            exp_varnames.append('time-all')

        return exp_varnames

    def getAnnotations(self, media_obj_id):
        """
        Returns all annotations and summary statistics for the given media
        object id.
        """
        if media_obj_id not in self.vals:
            raise KeyError(media_obj_id)

        res = {varname: '' for varname in self.getVarNames()}

        for varname, stats in self.vals[media_obj_id].items():
            res[varname + '-n'] = stats.getCountTotal()
            res[varname + '-majority'] = stats.getMajority(self.none_val)
            if self.include_plurality:
                res[varname + '-plurality'] = stats.getPlurality(self.none_val)

            all_vals = []
            for val in stats.getAnnotVals():
                all_vals.append('"{0}"({1})'.format(val, stats[val]))

            res[varname + '-all'] = ','.join(all_vals)
        
        if media_obj_id in self.times:
            res['time-n'] = len(self.times[media_obj_id])
            res['time-mean'] = (
                sum(self.times[media_obj_id]) / len(self.times[media_obj_id])
            )
            res['time-min'] = (min(self.times[media_obj_id]))
            res['time-max'] = (max(self.times[media_obj_id]))
            res['time-all'] = ','.join(
                [str(t) for t in self.times[media_obj_id]]
            )

        return res

