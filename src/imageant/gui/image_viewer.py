# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
import time
from PyQt5.QtWidgets import (
    QScrollArea, QLabel, QSizePolicy, QScroller, QAbstractSlider
)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QGuiApplication, QPixmap, QPalette, QKeySequence
from .mobj_viewer_abc import MediaObjectViewer


class ImageViewer(QScrollArea, MediaObjectViewer):
    # Define the zoom modes.
    ZOOM_CUSTOM = 0
    ZOOM_FIT = 1

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setBackgroundRole(QPalette.Dark)

        self.imglabel = QLabel()
        self.imglabel.setScaledContents(True)

        # One way to implement "fit to window" is to set the label's size
        # policy to Ignored and setWidgetResizable(True).  However, this will
        # scale the label to fill the dimensions of the window, typically
        # resulting in distortion, so "fit to window" is implemented manually
        # to preserve image aspect ratio.
        self.imglabel.setSizePolicy(
            QSizePolicy.Preferred, QSizePolicy.Preferred
        )
        self.setWidgetResizable(False)

        self.setAlignment(Qt.AlignCenter)
        self.setWidget(self.imglabel)

        QScroller.grabGesture(
            self.viewport(), QScroller.LeftMouseButtonGesture
        )

        self.zoom_mode = self.ZOOM_FIT
        self.zoom_factor = 1.0

        # Tracks the time that an image was loaded.
        self.img_start_time = None

    def resizeEvent(self, event):
        if (
            self.zoom_mode == self.ZOOM_FIT and
            self.imglabel.pixmap() is not None
        ):
            self.zoomToFit()

        super().resizeEvent(event)

    def getKeyboardShortcutsInfo(self):
        # Get the platform-specific string for the "Qt.CTRL" key.
        ctrl_str = QKeySequence(Qt.CTRL).toString(QKeySequence.NativeText)
        if ctrl_str[-1] == '+':
            ctrl_str = ctrl_str[:-1]

        return ((
            ('arrow keys', 'scroll'),
            ((ctrl_str + '+arrow keys').lower(), 'fast scroll')
        ),)

    def keyPressEvent(self, event):
        """
        Modifies the usual key press event handling for a QAbstractScrollArea
        so that it also allows Ctrl+arrow keys for scrolling by entire page
        steps.
        """
        # In the modifiers test, we need to remove the KeypadModifier flag
        # before testing for the control key, because on macOS, the arrow keys
        # set the KeypadModifier flag.
        modifiers = QGuiApplication.keyboardModifiers()
        if (modifiers & ~(Qt.KeypadModifier)) == Qt.ControlModifier:
            if event.key() == Qt.Key_Left:
                hsb = self.horizontalScrollBar()
                if hsb.value() > 0:
                    hsb.triggerAction(QAbstractSlider.SliderPageStepSub)
            elif event.key() == Qt.Key_Right:
                hsb = self.horizontalScrollBar()
                if hsb.value() < hsb.maximum():
                    hsb.triggerAction(QAbstractSlider.SliderPageStepAdd)
            elif event.key() == Qt.Key_Up:
                vsb = self.verticalScrollBar()
                if vsb.value() > 0:
                    vsb.triggerAction(QAbstractSlider.SliderPageStepSub)
            elif event.key() == Qt.Key_Down:
                vsb = self.verticalScrollBar()
                if vsb.value() < vsb.maximum():
                    vsb.triggerAction(QAbstractSlider.SliderPageStepAdd)
            else:
                super().keyPressEvent(event)
        else:
            super().keyPressEvent(event)

    def _getOptimalImageSize(self, pixmap):
        """
        Calculates the image size that fits optimally in the viewer's width and
        height.
        """
        size = pixmap.size()
        img_w = size.width()
        img_h = size.height()

        size = self.size()
        w = size.width()
        h = size.height()

        # First try vertically maximizing the image fit.
        if h > img_h:
            t_height = img_h
        else:
            t_height = h - self.frameWidth() * 2
        t_width = int(0.5 + (t_height * img_w) / img_h)

        # If that doesn't work, horizontally maximize the image fit.
        if t_width > w or t_width == 0:
            t_width = w - self.frameWidth() * 2
            t_height = int(0.5 + (t_width * img_h) / img_w)

        return (t_width, t_height)

    def loadMediaObject(self, imgpath):
        """
        Load and display a new image from an image file.
        """
        if not(os.path.isfile(imgpath)):
            raise Exception(
                'The image file "{0}" could not be found.'.format(imgpath)
            )

        # When auto-detecting image format, Qt first inspects the file
        # extension, and if the extension matches a known format, it does not
        # actually inspect the file contents.  Thus, if an image file has an
        # incorrect extension, the file will fail to load.  I have actually
        # seen this "in the wild" with real image files, so if auto-detection
        # fails, we try explicitly loading the image with the two most common
        # image formats in case the extension is wrong.
        pixmap = QPixmap(imgpath)
        if pixmap.isNull():
            pixmap = QPixmap(imgpath, 'JPG')
        if pixmap.isNull():
            pixmap = QPixmap(imgpath, 'PNG')

        if pixmap.isNull():
            raise Exception(
                'The image file "{0}" could not be loaded.'.format(imgpath)
            )

        self.imglabel.setPixmap(pixmap)

        self.zoomToFit()

        self.img_start_time = time.monotonic()

    def unloadMediaObject(self):
        self.imglabel.clear()
        self.img_start_time = None

    def getMediaObjTime(self):
        """
        Returns the total time in seconds that the current image has been
        displayed.
        """
        if self.img_start_time is None:
            return None
        else:
            return (time.monotonic() - self.img_start_time)

    def _getCenter(self):
        """
        Returns the center location of the visible portion of the image as a
        tuple of proportions, (prop_x, prop_y).
        """
        img_size = self.imglabel.size()
        v_size = self.viewport().size()

        if img_size.width() <= v_size.width():
            center_w = 0.5
        else:
            start_x = self.horizontalScrollBar().value()
            center_w = (start_x + (v_size.width() / 2)) / img_size.width()

        if img_size.height() <= v_size.height():
            center_h = 0.5
        else:
            start_y = self.verticalScrollBar().value()
            center_h = (start_y + (v_size.height() / 2)) / img_size.height()

        return (center_w, center_h)

    def _resizeView(self, new_w, new_h):
        # Save the center of the current image view so we can try to preserve
        # location when zooming in or out.
        center_x, center_y = self._getCenter()

        self.imglabel.resize(new_w, new_h)

        # If either image dimension exceeds the maximum viewable area, scroll
        # the view to try to preserve the center location.
        max_w = self.size().width() - self.frameWidth() * 2
        max_h = self.size().height() - self.frameWidth() * 2
        if new_w > max_w or new_h > max_h:
            # Note that the margin calculations here are not exactly correct
            # whenever the QScrollArea "turns on" a scrollbar because they
            # don't account for the change in viewport size caused by the
            # appearance of the scrollbar.  However, they seem to be close
            # enough in practice.  It should be possible to use the scrollbar's
            # sizeHint() information to make this more accurate, but I'm not
            # sure how this would behave on all platforms.
            v_size = self.viewport().size()
            self.ensureVisible(
                new_w * center_x, new_h * center_y,
                v_size.width() / 2, v_size.height() / 2
            )

    def zoomable(self):
        return True

    def zoom(self, zoom_mult):
        """
        Zooms the image by a given size multiplier.
        """
        if self.imglabel.pixmap() is None:
            return

        img_size = self.imglabel.size()
        pm_size = self.imglabel.pixmap().size()

        if self.zoom_mode == self.ZOOM_FIT:
            # Set the initial zoom factor and view mode.
            self.zoom_factor = img_size.height() / pm_size.height()
            self.zoom_mode = self.ZOOM_CUSTOM

        old_zoom_factor = self.zoom_factor
        self.zoom_factor *= zoom_mult

        # Calculate the new zoom factor, ensuring we don't zoom in beyond 1:1
        # or zoom out to an image dimension less than 20 px.
        if self.zoom_factor > 1.0:
            self.zoom_factor = 1.0
        elif (
            pm_size.width() * self.zoom_factor < 20 or
            pm_size.height() * self.zoom_factor < 20
        ):
            self.zoom_factor = old_zoom_factor

        if old_zoom_factor != self.zoom_factor:
            new_w = int(pm_size.width() * self.zoom_factor + 0.5)
            new_h = int(pm_size.height() * self.zoom_factor + 0.5)

            if new_w != img_size.width():
                self._resizeView(new_w, new_h)

    def zoomFullSize(self):
        self.zoom_mode = self.ZOOM_CUSTOM
        self.zoom_factor = 1.0

        pm_size = self.imglabel.pixmap().size()
        new_w = pm_size.width()
        new_h = pm_size.height()

        if new_w != self.imglabel.size().width():
            self._resizeView(new_w, new_h)

    def zoomToFit(self):
        """
        Zooms the image to fit optimally in the viewer's screen space.
        """
        if self.imglabel.pixmap() is None:
            return

        self.zoom_mode = self.ZOOM_FIT

        new_w, new_h = self._getOptimalImageSize(self.imglabel.pixmap())

        if new_w != self.imglabel.size().width():
            self.imglabel.resize(new_w, new_h)

