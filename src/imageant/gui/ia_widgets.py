# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Defines a few custom widgets used by the main GUI.
#

import os.path
from PyQt5.QtWidgets import (
    QLabel, QFrame, QMessageBox, QSpacerItem, QSizePolicy, QDialog, QLineEdit,
    QPushButton, QDialogButtonBox, QHBoxLayout, QVBoxLayout, QFileDialog,
    QDockWidget
)
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QGuiApplication, QPalette, QIcon


class IADockWidget(QDockWidget):
    """
    A dock widget that includes a thin border around the outside of the window
    and that accepts a layout for its contents.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # If a title was provided, use it to generate the object name.
        if len(args) > 0 and isinstance(args[0], str):
            self.setObjectName(args[0].lower().replace(' ', '_'))

        self.frame = QFrame()
        self.frame.setFrameStyle(QFrame.Box)
        self.frame.setLineWidth(1)
        self.frame.setFrameShadow(QFrame.Plain)
        self.frame.setAutoFillBackground(True)

        # Set the frame color to just slightly darker than the background
        # color.  Do this using stylesheets, not palette manipulation, so we
        # can avoid changes cascading to child widgets.
        bgcolor = self.frame.palette().color(self.frame.backgroundRole())
        fcolor = bgcolor.darker(120)
        self.setStyleSheet(
            'QDockWidget > QFrame '
            '{{ border: 1px solid {0}; }}'.format(fcolor.name())
        )
        self.setWidget(self.frame)

    def setFrameLayout(self, layout):
        self.frame.setLayout(layout)


class RichTextFrame(QLabel):
    """
    A frame/label for displaying rich text in a window or other large-ish area.
    It returns a size hint that tries to preserve a UI-appropriate size even if
    the label's text string is small, and it uses a default presentation
    appropriate for use as the main widget in a window.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)
        self.setBackgroundRole(QPalette.Window)
        self.setAutoFillBackground(True)
        self.setMargin(8)
        self.setWordWrap(True)
        #self.setScaledContents(True)

    def sizeHint(self):
        """
        Returns a size hint that tries to preserve a layout width sufficient
        for displaying at least ~14 ems in the default font.
        """
        default_val = super().sizeHint()

        preferred_r = self.fontMetrics().boundingRect('M' * 14)
        preferred_min = preferred_r.width() + self.margin() * 2

        if default_val.width() < preferred_min:
            new_size = QSize(preferred_min, default_val.height())

            return new_size
        else:
            return default_val

    def setBackgroundColor(self, colorstr):
        self.setStyleSheet('QLabel {{ background: {0}; }}'.format(colorstr))


class SmartMsgBox(QMessageBox):
    """
    A custom QMessageBox that provides better message layout.  By default,
    QMessageBox will wrap the informative text to about the same width as the
    main text (on X11/GTK, anyway), and this can lead to some ugly message
    boxes with overly aggressive wrapping.  This appears to be a well-known
    problem, with a variety of proposed solutions (e.g.,
    http://apocalyptech.com/linux/qt/qmessagebox/,
    https://www.qtcentre.org/threads/22298-QMessageBox-Controlling-the-width).

    The approach taken here is to use a spacer in the last two columns of the
    QMessageBox's grid layout to force the message area to have a minimum
    width.  The minimum width is calculated dynamically based upon the screen
    size, the main text size, and the informative text size.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def setInformativeText(self, inf_text):
        super().setInformativeText(inf_text)

        target_min = 500
        scr_width = QGuiApplication.primaryScreen().availableSize().width()
        if scr_width < target_min:
            target_min = scr_width

        t_width = self.fontMetrics().boundingRect(self.text()).width()
        it_width = self.fontMetrics().boundingRect(inf_text).width()

        if t_width < it_width:
            if it_width < target_min - 20:
                sp_width = it_width + 20
            else:
                sp_width = target_min

            h_spacer = QSpacerItem(
                sp_width, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Fixed
            )
            layout = self.layout()
            layout.addItem(
                h_spacer, layout.rowCount(), 1, 1, layout.columnCount() - 1
            )


class FileConfirmDialog(QDialog):
    """
    Implements a dialog for suggesting a file name and allowing the user to
    edit it directly or choose a file name with a file chooser dialog.  The
    interface of the dialog is similar to GTK+'s GtkFileChooserButton.
    """
    def __init__(self, text='', init_path='', parent=None):
        super().__init__(parent)

        self.setModal(True)

        msg_label = QLabel(text)
        msg_label.setWordWrap(True)

        self.path_disp = QLineEdit(init_path, self)

        # Make the path display at least ~20 ems wide in the default font.
        min_r = self.path_disp.fontMetrics().boundingRect('M' * 20)
        margins = self.path_disp.textMargins()
        self.path_disp.setMinimumWidth(
            min_r.width() + margins.left() + margins.right()
        )

        f_button = QPushButton(
            QIcon.fromTheme('document-open'), '&Choose file...', self
        )
        f_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        f_button.clicked.connect(self._chooseFileClicked)

        buttonbox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)
        buttonbox.accepted.connect(self.accept)
        buttonbox.rejected.connect(self.reject)

        h_layout = QHBoxLayout()
        h_layout.addWidget(self.path_disp)
        h_layout.addWidget(f_button)

        v_layout = QVBoxLayout()
        v_layout.addStretch()
        v_layout.addWidget(msg_label)
        v_layout.addSpacing(8)
        v_layout.addLayout(h_layout)
        v_layout.addSpacing(18)
        v_layout.addWidget(buttonbox)
        v_layout.addStretch()

        self.setLayout(v_layout)

    def _isValidFilePath(self, fpath):
        if fpath == '':
            msg = (
                'The output file path is empty.'.format(fpath)
            )
            return (False, msg)

        fpath = os.path.abspath(fpath)

        if not(os.path.isdir(os.path.dirname(fpath))):
            msg = (
                'The output file path "{0}" is not valid because one or more '
                'of the folders in the path does not exist.'.format(fpath)
            )
            return (False, msg)

        if os.path.isdir(fpath):
            msg = (
                'The output file path "{0}" is not valid because a folder with '
                'the same name already exists.'.format(fpath)
            )
            return (False, msg)

        return (True, '')

    def accept(self):
        fpath = self.getFilePath().strip()

        isvalid = self._isValidFilePath(fpath)
        if not(isvalid[0]):
            msgbox = SmartMsgBox(
                QMessageBox.Warning, '', 'Invalid file path.',
                QMessageBox.Ok, self
            )
            msgbox.setInformativeText(isvalid[1])
            msgbox.exec()
        else:
            if os.path.exists(fpath) and os.path.getsize(fpath) > 0:
                resp = QMessageBox.question(
                    self, '', 'The file "{0}" is not empty.  If you use it as '
                    'the output file, the existing contents could change.  '
                    'Use it anyway?'.format(os.path.basename(fpath))
                )
                if resp == QMessageBox.Yes:
                    super().accept()
            else:
                super().accept()

    def getFilePath(self):
        return os.path.abspath(self.path_disp.text().strip())

    def _chooseFileClicked(self):
        fpath = QFileDialog.getSaveFileName(
            self, 'Choose an output CSV file name', self.path_disp.text(),
            'CSV files (*.csv);;All files(*)', None,
            QFileDialog.DontConfirmOverwrite
        )[0]

        if fpath != '':
            self.path_disp.setText(fpath)

