# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtGui import QGuiApplication, QColor, QPalette


# These are the 12 colors suggested by C. Ware (2012) in Information
# Visualization: Perception for Design.  I obtained the RGB values directly
# from a vector graphic in the PDF version of Ware (2012).
# Color order: [
#     red, green, yellow, blue, black, white,
#     pink, cyan, gray, orange, brown, purple
# ].
c_ware_colors = [
    '#eb002f', '#1ac700', '#faee00', '#0029fa', '#000000', '#ffffff',
    '#ff66ae', '#33f9ff', '#7a8075', '#ff8533', '#8a1b00', '#85008a'
]


def getBackgroundColors(reduced=True, l_threshold=240):
    """
    Returns a list of contrasting background colors intended to be used under
    dark text.  The color of the Window role in the default application palette
    will always be the first color.  To get this value, an instance of
    QApplication must be active, which means that this function should *only*
    be called after creating an instance of QApplication.  If reduced is True,
    a subset of images is returned that are easily distinguished after
    adjusting lightness.
    """
    bg_colors = [
        QGuiApplication.palette().color(QPalette.Window).name()
    ]

    if reduced:
        # Only use colors that are easily distinguished after adjusting the
        # lightness.
        color_is = [1, 2, 3, 0, 5, 7, 9]
    else:
        color_is = range(len(c_ware_colors))

    for i in color_is:
        color = QColor(c_ware_colors[i])
        h, s, l, a = color.getHsl()

        new_l = l_threshold
        if l > new_l:
            new_l = l

        new_colorstr = QColor.fromHsl(h, s, new_l).name()

        if new_colorstr != bg_colors[0]:
            bg_colors.append(new_colorstr)

    return bg_colors


if __name__ == '__main__':
    #
    # Show a simple window with examples of all background colors and print the
    # color values to the console.
    #
    import sys
    from ia_widgets import RichTextFrame
    from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout

    app = QApplication(sys.argv)

    #bg_colors = getBackgroundColors(False, 230)
    bg_colors = getBackgroundColors(True, 240)
    print(bg_colors)

    demo = QWidget()
    layout = QGridLayout()

    for cnt, color in enumerate(bg_colors):
        label = RichTextFrame(
            '<p style="font-size: large;">Example task.</p>'
            '<p>Option 1</p><p>Option 2</p>'
        )
        label.setBackgroundColor(color)
        layout.addWidget(label, cnt // 4, cnt % 4)

    demo.setLayout(layout)
    demo.show()
    app.exec_()

