# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
import html
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QSize, QFile, QSettings
from PyQt5.QtGui import QGuiApplication, QPalette, QKeySequence, QIcon
from .ia_widgets import (
    RichTextFrame, SmartMsgBox, FileConfirmDialog, IADockWidget
)
from .new_session_dialog import NewSessionDialog
from .empty_viewer import EmptyMediaViewer
from .image_viewer import ImageViewer
from .audio_viewer import AudioViewer
from ..task_script import TaskResponse
from .color_labels import getBackgroundColors
from . import ia_icons
from ..annot_session import AnnotationSession


class ImageAntUI(QMainWindow):
    """
    Implements the top-level GUI for ImageAnt, based on QMainWindow.  The
    central widget for the QMainWindow will be a media-type specific media
    viewer that must implement the following interface:
        getShortcutsHelpStr()
        loadMediaObject(mfile_path)
        unloadMediaObject()
        getMediaObjTime()
        zoomable()
        zoom(zoom_mult)
        zoomFullSize()
        zoomToFit()

    """
    MAX_RECENT_SESSIONS = 5

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.session = None
        self.runner = None
        self.cur_task = None
        self.resp_keys = []
        self.resp_ids_map = {}
        self.cur_mobj = None

        self.next_color = 0
        self.color_map = {}

        self.settings = QSettings()
        #print(self.settings.fileName())

        self.setWindowTitle(QApplication.applicationName())

        self._createDocks()
        self.task_bg_colors = getBackgroundColors()

        # A map to manage MediaObjectViewer instances and ensure we only create
        # one instance of a viewer for a given media type.
        self.media_viewers = {
            AnnotationSession.MEDIA_IMAGES: None,
            AnnotationSession.MEDIA_AUDIO: None
        }

        # To manage the media viewer central widget, we set up a simple QWidget
        # with a QBoxLayout and use the layout to manage whatever media viewer
        # is active.  The advantage of this approach is that the QMainWindow's
        # central widget never needs to change, and the layout automatically
        # handles sizing media viewers when the media type changes.
        central_widget = QWidget()
        self.central_layout = QHBoxLayout()
        self.central_layout.setContentsMargins(0, 0, 0, 0)
        central_widget.setLayout(self.central_layout)

        self.media_viewer = EmptyMediaViewer()
        self.central_layout.addWidget(self.media_viewer)
        self.setCentralWidget(central_widget)

        #print(QFile.exists(':/icons/imageant/index.theme'))
        #print(QIcon.themeSearchPaths())
        QIcon.setThemeName('imageant')
        self._createFileActions()
        self._createEditActions()
        self._createViewActions()

        self.annotations_active = False
        self.task_frame.setEnabled(False)
        self.shortcuts_msg_frame.setEnabled(False)

        self._initAnnotationSession()

        self._setGeomConfig()

        self._updateRecentSessions(None)

    #
    # Private UI setup methods.
    #
    def _createDocks(self):
        self.task_dock = IADockWidget('Annotation task')
        self.task_frame = RichTextFrame()
        self.task_frame.setText('No active annotation task.')
        vbox = QVBoxLayout()
        vbox.addWidget(self.task_frame)
        vbox.addStretch()
        self.task_dock.setFrameLayout(vbox)
        self.addDockWidget(Qt.RightDockWidgetArea, self.task_dock)
        #print(vbox.contentsMargins().right())

        self.shortcuts_dock = IADockWidget('Keyboard shortcuts')
        self.shortcuts_msg_frame = RichTextFrame()
        self.shortcuts_msg_frame.setText('No active annotation session.')
        vbox = QVBoxLayout()
        vbox.addWidget(self.shortcuts_msg_frame)
        vbox.addStretch()
        self.shortcuts_dock.setFrameLayout(vbox)
        self.addDockWidget(Qt.RightDockWidgetArea, self.shortcuts_dock)

        self.progress_dock = IADockWidget('Progress')
        vbox = QVBoxLayout()
        self.prog_label = QLabel('No media objects loaded.')
        self.prog_label.setAlignment(Qt.AlignLeft)
        vbox.addWidget(self.prog_label)
        self.mobj_name_le = QLineEdit('')
        self.mobj_name_le.setReadOnly(True)
        self.mobj_name_le.setFocusPolicy(Qt.NoFocus)
        self.mobj_name_le.setStyleSheet('background: transparent; border: 0px;')
        vbox.addWidget(self.mobj_name_le)
        self.progbar = QProgressBar()
        vbox.addWidget(self.progbar)
        vbox.addStretch()
        self.progress_dock.setFrameLayout(vbox)
        self.addDockWidget(Qt.RightDockWidgetArea, self.progress_dock)

    def _getShortcutsStr(self, mediastr):
        margin_i = 6
        margin_g = 16

        shortcuts = []

        if self.media_viewer.zoomable():
            shortcuts.extend([
                ['=</b>, <b>+', 'zoom in', margin_i],
                ['-', 'zoom out', margin_i],
                ['x', 'zoom to fit', margin_i],
                ['z', 'zoom to full size', margin_g]
            ])

        shortcuts.extend([
            [
                self.undo_task_action.shortcut().toString(
                    QKeySequence.NativeText
                ).lower(),
                'undo last annotation', margin_i
            ],
            [
                self.undo_mobj_action.shortcut().toString(
                    QKeySequence.NativeText
                ).lower(),
                'undo last {0}'.format(mediastr), margin_i
            ]
        ])

        # Process the viewer-specific shortcut key information.
        viewer_shortcuts = self.media_viewer.getKeyboardShortcutsInfo()
        for shortcuts_grp in viewer_shortcuts:
            shortcuts[-1][2] = margin_g
            for sc_info in shortcuts_grp:
                shortcuts.append([sc_info[0], sc_info[1], margin_i])

        sc_str = ''
        template = ('<p style="margin-left: 8px; margin-top: 0px; '
        'margin-bottom: {2}px"><b>{0}</b> : {1}</p>')

        for shortcut in shortcuts:
            sc_str += template.format(shortcut[0], shortcut[1], shortcut[2])

        return sc_str

    def _createFileActions(self):
        self.toolbar = QToolBar('Toolbar', self)
        self.toolbar.setObjectName('toolbar')
        self.addToolBar(Qt.LeftToolBarArea, self.toolbar)

        filemenu = self.menuBar().addMenu('&File')

        action = QAction(
            QIcon.fromTheme('annotation-session-image-new'),
            '&New image annotation session...', self
        )
        action.triggered.connect(
            lambda is_checked:
                self._newAnnotationSession(AnnotationSession.MEDIA_IMAGES)
        )
        filemenu.addAction(action)
        self.toolbar.addAction(action)

        action = QAction(
            QIcon.fromTheme('annotation-session-audio-new'),
            '&New audio annotation session...', self
        )
        action.triggered.connect(
            lambda is_checked:
                self._newAnnotationSession(AnnotationSession.MEDIA_AUDIO)
        )
        filemenu.addAction(action)
        self.toolbar.addAction(action)

        action = QAction(
            QIcon.fromTheme('annotation-session-restore'),
            '&Restore annotation session...', self
        )
        action.triggered.connect(self._restoreAnnotationSession)
        filemenu.addAction(action)
        self.toolbar.addAction(action)

        filemenu.addSeparator()
        self.toolbar.addSeparator()
        self.save_action = QAction(
            QIcon.fromTheme('document-save'), '&Save annotations', self
        )
        self.save_action.setShortcut(QKeySequence.Save)
        self.save_action.triggered.connect(self._saveAnnotations)
        filemenu.addAction(self.save_action)
        self.toolbar.addAction(self.save_action)

        # Create the "recent sessions" actions.
        filemenu.addSeparator()
        self.recent_sess_actions = []
        for cnt in range(self.MAX_RECENT_SESSIONS):
            s_action = QAction(self)
            s_action.setVisible(False)
            s_action.triggered.connect(self._openRecentAnnotationSession)
            filemenu.addAction(s_action)
            self.recent_sess_actions.append(s_action)

        filemenu.addSeparator()
        filemenu.addAction(
            QIcon.fromTheme('application-exit'), 'E&xit', self.close,
            QKeySequence.Quit
        )

        self.save_action.setEnabled(False)

    def _createEditActions(self):
        editmenu = self.menuBar().addMenu('&Edit')
        self.toolbar.addSeparator()

        self.undo_task_action = QAction(
            QIcon.fromTheme('edit-undo'), '&Undo last annotation', self
        )
        self.undo_task_action.setShortcut(QKeySequence.Undo)
        self.undo_task_action.triggered.connect(self._undoTaskTriggered)
        editmenu.addAction(self.undo_task_action)
        self.toolbar.addAction(self.undo_task_action)

        self.undo_mobj_action = QAction(
            QIcon.fromTheme('edit-undo-image'), '&Undo last media object', self
        )
        self.undo_mobj_action.setShortcut(
            QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_Z)
        )
        self.undo_mobj_action.triggered.connect(self._undoMediaObjTriggered)
        editmenu.addAction(self.undo_mobj_action)
        self.toolbar.addAction(self.undo_mobj_action)

        self.undo_task_action.setEnabled(False)
        self.undo_mobj_action.setEnabled(False)

    def _createViewActions(self):
        viewmenu = self.menuBar().addMenu('&View')
        self.toolbar.addSeparator()

        self.zoom_actions = QActionGroup(self)

        action = QAction(
            QIcon.fromTheme('zoom-in'), 'Zoom &in', self.zoom_actions
        )
        action.setShortcuts(['=', '+'])
        action.triggered.connect(self._zoomIn)
        viewmenu.addAction(action)
        self.toolbar.addAction(action)

        action = QAction(
            QIcon.fromTheme('zoom-out'), 'Zoom &out', self.zoom_actions
        )
        action.setShortcut('-')
        action.triggered.connect(self._zoomOut)
        viewmenu.addAction(action)
        self.toolbar.addAction(action)

        action = QAction(
            QIcon.fromTheme('zoom-fit-best'), 'Zoom to &fit',
            self.zoom_actions
        )
        action.setShortcut('x')
        action.triggered.connect(self._zoomToFit)
        viewmenu.addAction(action)
        self.toolbar.addAction(action)

        action = QAction(
            QIcon.fromTheme('zoom-original'), 'Zoom f&ull size',
            self.zoom_actions
        )
        action.setShortcut('z')
        action.triggered.connect(self._zoomFullSize)
        viewmenu.addAction(action)
        self.toolbar.addAction(action)

        self.zoom_actions.setEnabled(False)

        viewmenu.addSeparator()

        action = QAction('Annotation &task window', self)
        action.setCheckable(True)
        action.setChecked(True)
        action.triggered.connect(self.task_dock.setVisible)
        self.task_dock.visibilityChanged.connect(action.setChecked)
        viewmenu.addAction(action)

        action = QAction('&Shortcuts window', self)
        action.setCheckable(True)
        action.setChecked(True)
        action.triggered.connect(self.shortcuts_dock.setVisible)
        self.shortcuts_dock.visibilityChanged.connect(action.setChecked)
        viewmenu.addAction(action)

        action = QAction('&Progress window', self)
        action.setCheckable(True)
        action.setChecked(True)
        action.triggered.connect(self.progress_dock.setVisible)
        self.progress_dock.visibilityChanged.connect(action.setChecked)
        viewmenu.addAction(action)

        viewmenu.addSeparator()

        action = QAction('Tool&bar', self)
        action.setCheckable(True)
        action.setChecked(True)
        action.triggered.connect(self.toolbar.setVisible)
        self.toolbar.visibilityChanged.connect(action.setChecked)
        viewmenu.addAction(action)

    def _setGeomConfig(self):
        saved_geom = self.settings.value('geometry')
        saved_config = self.settings.value('window_config')

        if saved_geom is None:
            scr_size = QGuiApplication.primaryScreen().availableSize()
            self.resize(scr_size.width() * 0.8, scr_size.height() * 0.9)
            fr_rect = self.frameGeometry()
            self.move(
                (scr_size.width() - fr_rect.width()) / 2,
                (scr_size.height() - fr_rect.height()) / 2
            )
        else:
            self.restoreGeometry(saved_geom)

        if saved_config is None:
            # Set an initial width for the side docks so they don't "jump" in
            # horizontal size between annotation tasks as size hints change.
            dock_width = self.task_dock.sizeHint().width()
            if dock_width < self.width() // 6:
                dock_width += 40
            self.resizeDocks([self.task_dock], [dock_width], Qt.Horizontal)

            # Set the relative heights of the dock windows so that the
            # annotation task dock gets all extra space.
            self.resizeDocks(
                [self.task_dock, self.shortcuts_dock, self.progress_dock],
                [100, 1, 1], Qt.Vertical
            )
        else:
            self.restoreState(saved_config)

    #
    # Standard dialog convenience methods.
    #
    def _showMsg(self, text, inf_text, msgtype=QMessageBox.Information):
        msgbox = SmartMsgBox(
            msgtype, '', text, QMessageBox.Ok, self
        )
        msgbox.setInformativeText(inf_text)
        msgbox.exec()

    def _askYesNo(self, text, inf_text, msgtype=QMessageBox.Question):
        """
        Prompts the user with a yes/no question and returns the response as a
        QMessageBox StandardButton value.
        """
        msgbox = SmartMsgBox(
            msgtype, '', text, QMessageBox.Yes | QMessageBox.No, self
        )
        msgbox.setInformativeText(inf_text)
        msgbox.setDefaultButton(QMessageBox.Yes)

        return msgbox.exec()

    def _askYesNoCancel(self, text, inf_text, msgtype=QMessageBox.Question):
        """
        Prompts the user with a yes/no/cancel question and returns the
        response as a QMessageBox StandardButton value.
        """
        msgbox = SmartMsgBox(
            msgtype, '', text,
            QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, self
        )
        msgbox.setInformativeText(inf_text)
        msgbox.setDefaultButton(QMessageBox.Yes)

        return msgbox.exec()

    #
    # Event handlers.
    #
    def closeEvent(self, event):
        if self.cur_mobj is not None:
            # Log the display time of the current media object before closing.
            self.runner.updateMediaObjTime(self.media_viewer.getMediaObjTime())

        self.settings.setValue('geometry', self.saveGeometry())
        self.settings.setValue('window_config', self.saveState())

        event.accept()

    def keyPressEvent(self, event):
        keyval = event.text()
        if self.annotations_active and keyval in self.resp_keys:
            self._handleAnnotationResponse(keyval)
        else:
            super().keyPressEvent(event)

    #
    # Widget signal handlers.
    #
    def _newAnnotationSession(self, media_type):
        diag = NewSessionDialog(media_type, self)
        if self.session is not None:
            diag.initFilePaths(
                self.session.getSessionFile(), self.session.getScriptFile(),
                self.session.getMediaFolder()
            )

        response = diag.exec()

        if response == QDialog.Accepted:
            self.newSession(
                diag.getSessionFilePath(), diag.getScriptFilePath(),
                diag.getMediaFolderPath(), media_type
            )

    def _restoreAnnotationSession(self):
        filepath = QFileDialog.getOpenFileName(
            self, 'Restore annotation session', '.',
            'Annotation session files (*.ia_sess);;All files (*)'
        )[0]

        if filepath != '':
            self.restoreSession(filepath)

    def _openRecentAnnotationSession(self):
        rs_action = self.sender()
        self.restoreSession(rs_action.data())

    def _undoTaskTriggered(self):
        self.runner.undoLastTask(self.media_viewer.getMediaObjTime())

    def _undoMediaObjTriggered(self):
        self.runner.undoLastMediaObject(self.media_viewer.getMediaObjTime())

    def _zoomIn(self):
        self.media_viewer.zoom(2.0)

    def _zoomOut(self):
        self.media_viewer.zoom(0.5)

    def _zoomToFit(self):
        self.media_viewer.zoomToFit()

    def _zoomFullSize(self):
        self.media_viewer.zoomFullSize()

    #
    # Other "private" methods.
    #
    def _initAnnotationSession(self):
        self.session = AnnotationSession()
        self.session.mediaTypeChange.registerObserver(self.setMediaType)

        self.runner = self.session.getRunner()
        self.runner.taskUndoStateChange.registerObserver(
            self.setUndoTaskEnabled
        )
        self.runner.mediaObjUndoStateChange.registerObserver(
            self.setUndoMediaObjEnabled
        )
        self.runner.saveStateChange.registerObserver(
            self.updateSaveState
        )
        self.runner.annotationTaskChange.registerObserver(
            self.updateAnnotationTask
        )
        self.runner.mediaObjectChange.registerObserver(
            self.updateMediaObj
        )
        self.runner.annotationStateChange.registerObserver(
            self.setAnnotationsEnabled
        )
        self.runner.annotationsCompleted.registerObserver(
            self.annotationsComplete
        )

    def _updateRecentSessions(self, sess_file):
        """
        Retrieves the list of recent sessions, removes any invalid entries,
        optionally adds a new entry, and updates the menu actions accordingly.
        """
        sess_list = self.settings.value('recent_sessions', [])
        # We can still get None back if the payload is an invalid value.
        if sess_list is None:
            sess_list = []

        if sess_file is not None:
            # Update the session list with the provided file path.
            try:
                sess_list.remove(sess_file)
            except ValueError:
                pass
            sess_list.insert(0, sess_file)

        # Remove any file paths that no longer exist.
        for i in range(len(sess_list) - 1, -1, -1):
            if not(os.path.exists(sess_list[i])):
                del sess_list[i]

        if len(sess_list) > self.MAX_RECENT_SESSIONS:
            sess_list = sess_list[0:self.MAX_RECENT_SESSIONS]

        self.settings.setValue('recent_sessions', sess_list)

        # Update the menu actions.
        i = -1
        for i in range(len(sess_list)):
            self.recent_sess_actions[i].setData(sess_list[i])
            act_text = '&{0}. {1}'.format(
                i + 1, os.path.basename(sess_list[i])
            )
            self.recent_sess_actions[i].setText(act_text)
            self.recent_sess_actions[i].setVisible(True)

        for j in range(i + 1, self.MAX_RECENT_SESSIONS):
            self.recent_sess_actions[j].setVisible(False)

    def _handleAnnotationResponse(self, keyval):
        """
        Processes a user annotation response, which is provided as the key the
        user pressed.
        """
        try:
            self.runner.setResponse(
                self.resp_ids_map[keyval], self.media_viewer.getMediaObjTime()
            )
        except Exception as err:
            raise
            self.setAnnotationsEnabled(False)
            self._showMsg(
                'Error running anotations script.', str(err),
                QMessageBox.Warning
            )

    def _setAnnotationTask(self, task):
        self.cur_task = task
        self.resp_keys = ['s']
        self.resp_ids_map = {'s': TaskResponse.RESPONSE_SKIP}

        task_str = (
            '<p style="font-size: large; font-weight: normal">' +
            html.escape(task.text) + '</p>'
        )

        resp_template = '<p style="margin-left: 10px;"><b>{0}</b> : {1}</p>'

        for resp in task.responses:
            if resp.response_type == TaskResponse.KEYPRESS:
                task_str += resp_template.format(
                    resp.key_val, html.escape(resp.text)
                )
                self.resp_keys.append(resp.key_val)
                self.resp_ids_map[resp.key_val] = resp.id

        task_str += resp_template.format('s', '<i>skip this {0}</i>'.format(
            self.session.getMediaTypeStr()
        ))

        if self.cur_task.varname not in self.color_map:
            self.color_map[self.cur_task.varname] = self.task_bg_colors[self.next_color]
            self.next_color = (self.next_color + 1) % len(self.task_bg_colors)

        self.task_frame.setText(task_str)
        self.task_frame.setBackgroundColor(self.color_map[self.cur_task.varname])

    def _unloadAnnotationTask(self):
        self.cur_task = None
        self.resp_keys.clear()
        self.resp_ids_map.clear()

        self.task_frame.setText('No active annotation task.')
        self.task_frame.setBackgroundColor(self.task_bg_colors[0])

        self.setAnnotationsEnabled(False)

    def _updateProgbar(self):
        """
        Updates the annotations progress bar to match the current state of the
        AnnotationsRunner.
        """
        if self.runner.getMediaObjCount() != self.progbar.maximum():
            self.progbar.setRange(0, self.runner.getMediaObjCount())

        self.progbar.setValue(self.runner.getAnnotatedMediaObjCount())

    def _updateMediaObj(self, mfile_path):
        """
        Update the displayed media object and progress indicator (if needed).
        """
        try:
            self.media_viewer.loadMediaObject(mfile_path)
            self.cur_mobj = mfile_path
        except Exception as err:
            self.setAnnotationsEnabled(False)
            self._showMsg(
                'Error loading {0}.'.format(
                    self.session.getMediaTypeStr()
                ), str(err), QMessageBox.Warning
            )
            return

        self._updateProgbar()

        mobj_num = self.runner.getAnnotatedMediaObjCount()

        titlestr = '{0} ({1} of {2}) - {3}'.format(
            os.path.basename(mfile_path), mobj_num + 1,
            self.progbar.maximum(),
            QApplication.applicationName()
        )
        self.setWindowTitle(titlestr)

        self.prog_label.setText(
            'Current {0} ({1} of {2}):'.format(
                self.session.getMediaTypeStr(),
                mobj_num+1, self.progbar.maximum()
            )
        )
        self.mobj_name_le.setText(os.path.basename(mfile_path))

        if not(self.zoom_actions.isEnabled()) and self.media_viewer.zoomable():
            self.zoom_actions.setEnabled(True)

    def _unloadMediaObject(self):
        self.cur_mobj = None
        self.media_viewer.unloadMediaObject()

        self.setWindowTitle(QApplication.applicationName())
        self.mobj_name_le.setText('')

        self.zoom_actions.setEnabled(False)
        self.setAnnotationsEnabled(False)

    def _saveAnnotations(self):
        self.runner.writeAnnotations()

    def _getMediaViewerInstance(self, media_type):
        if media_type == AnnotationSession.MEDIA_IMAGES:
            return ImageViewer()
        elif media_type == AnnotationSession.MEDIA_AUDIO:
            return AudioViewer()
        else:
            raise Exception('Unknown media type.')

    #
    # Public methods that also respond to events from an AnnotationSession or
    # an AnnotationsRunner.
    #
    def setMediaType(self, media_type):
        """
        Configures the UI for the current media type of the annotation session.
        """
        if self.media_viewers[media_type] is None:
            self.media_viewers[media_type] = self._getMediaViewerInstance(
                media_type
            )

        prev_viewer = self.media_viewer
        self.media_viewer = self.media_viewers[media_type]
        self.central_layout.replaceWidget(prev_viewer, self.media_viewer)
        self.media_viewer.show()
        prev_viewer.hide()

        mediastr = self.session.getMediaTypeStr()
        if mediastr == 'audio':
            mediastr += ' file'
        self.undo_mobj_action.setText('&Undo last {0}'.format(mediastr))

        self.shortcuts_msg_frame.setText(self._getShortcutsStr(mediastr))

    def setUndoTaskEnabled(self, enabled):
        self.undo_task_action.setEnabled(enabled)

    def setUndoMediaObjEnabled(self, enabled):
        self.undo_mobj_action.setEnabled(enabled)

    def updateSaveState(self, enabled):
        self.save_action.setEnabled(enabled)

    def updateAnnotationTask(self, task):
        """
        Responds to task change events of an AnnotationsRunner.

        task: An instance of AnnotationTask (or None).
        """
        if self.cur_task != task:
            if task is None:
                self._unloadAnnotationTask()
            else:
                self._setAnnotationTask(task)

    def updateMediaObj(self, mfile_path):
        """
        Responds to media object change events of an AnnotationsRunner.

        mfile_path: A path to a media file (or None).
        """
        if self.cur_mobj != mfile_path:
            if mfile_path is None:
                self._unloadMediaObject()
            else:
                self._updateMediaObj(mfile_path)

    def setAnnotationsEnabled(self, enabled):
        """
        Sets whether the UI will accept annotations from the user.
        """
        if self.annotations_active == enabled:
            return

        if enabled and (self.cur_task is None or self.cur_mobj is None):
            raise RuntimeError(
                'Cannot start annotating before media objects and an '
                'annotation script are loaded.'
            )

        self.annotations_active = enabled
        self.task_frame.setEnabled(enabled)
        self.shortcuts_msg_frame.setEnabled(enabled)

    def annotationsComplete(self):
        """
        Signals that all annotations for the current media object set are
        complete.
        """
        self._updateProgbar()
        self.prog_label.setText('Annotations complete.')

        result = self._askYesNo(
            'There are no more {0}s to annotate.'.format(
                self.session.getMediaTypeStr()
            ),
            'Do you want to save all {0} annotations to the file "{1}"?'.format(
                self.session.getMediaTypeStr(),
                os.path.basename(self.runner.getOutputFile())
            )
        )
        if result == QMessageBox.Yes:
            self._saveAnnotations()

    #
    # Additional public methods.
    #
    def newSession(self, sess_file, script_path, media_folder, media_type):
        try:
            self.session.updateSession(
                sess_file, script_path, media_folder, media_type
            )
        except Exception as err:
            self._showMsg(
                'Error creating new annotation session.', str(err),
                QMessageBox.Warning
            )
            return

        self._updateRecentSessions(self.session.getSessionFile())

    def restoreSession(self, sess_file):
        """
        sess_file (str): Path to an annotation session file.
        """
        try:
            self.session.restoreSession(sess_file)
        except Exception as err:
            self._showMsg(
                'Error restoring annotation session.', str(err),
                QMessageBox.Warning
            )
            return

        self._updateRecentSessions(self.session.getSessionFile())

