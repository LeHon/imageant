#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# A simple script to make it easier to delete icons from a theme or context.
#
# Usage: rm_icons.py FILE_NAME FOLDER
#
# FOLDER should contain icons files organized int subfolders according to
# target pixel dimensions (16/, 22/, 24/, etc.), and rm_icons.py will remove
# icon files from each size subfolder that match FILE_NAME.
#

import sys
import os
import os.path

sizes = [16, 22, 24, 32, 48, 64, 96, 128]

for size in sizes:
    fpath = os.path.join(sys.argv[2], str(size), sys.argv[1])
    if os.path.exists(fpath):
        print('Deleting {0}...'.format(fpath))
        try:
            os.unlink(fpath)
        except Exception as err:
            print(err)

