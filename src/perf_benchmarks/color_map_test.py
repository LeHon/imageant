#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Tests four methods for creating an off-screen buffer image of a spectrogram,
# and tests each of the non-numpy methods using numpy array and python list
# versions of the spectrogram data.
#
# The results with CPython 3.6.9 on GNU/Linux, using an 18-second, 44.1 kHz
# test .wav file (dtmf_tones.wav), 1024 window size, 50% window overlap, no
# start or end padding, and "audacity" color mapping (best time in seconds out
# of 20 trials):
#   multi-value mapping: 0.015606879955157638
#   qpixmap, numpy array: 4.485669138841331
#   qimage, numpy array: 4.421475499868393
#   bytearray, numpy array: 3.546647547977045
#   list conversion time: 0.026153889019042253
#   qpixmap, python list: 1.9198581869713962
#   qimage, python list: 1.9120983108878136
#   bytearray, python list: 1.1265238779596984
#

from package_context import imageant

import timeit, time
import librosa
from PyQt5.QtWidgets import QApplication
import sys
from imageant.audio.spectrogram import Spectrogram
from imageant.audio.color_mapper import ColorMapper


a_data, samp_rate = librosa.core.load('dtmf_tones.wav', sr=None, mono=True)
spec = Spectrogram()
spec.setInputData(a_data)
sdata, timeinfo = spec.getSpectrogramData()
cmapper = ColorMapper()

def _qpixmap():
    img = cmapper.getMappedImgData_QPixmap(sdata, 0, 0) 

def _qimage():
    img = cmapper.getMappedImgData_QImage(sdata, 0, 0) 

def _bytearray():
    img = cmapper.getMappedImgData_Bytearray(sdata, 0, 0) 

def _multi():
    img = cmapper.getMappedImgData_Multi(sdata, 0, 0)
    # Clear the cached color mapping values.
    cmapper._colormap_vals = None


# Required for using QPixmap and QImage.
app = QApplication(sys.argv)

reps = 20

res = timeit.repeat(_multi, repeat=reps, number=1)
print('multi-value mapping:', min(res))

res = timeit.repeat(_qpixmap, repeat=reps, number=1)
print('qpixmap, numpy array:', min(res))

res = timeit.repeat(_qimage, repeat=reps, number=1)
print('qimage, numpy array:', min(res))

res = timeit.repeat(_bytearray, repeat=reps, number=1)
print('bytearray, numpy array:', min(res))

stime = time.perf_counter()
sdata = sdata.tolist()
print('list conversion time:', time.perf_counter() - stime)

res = timeit.repeat(_qpixmap, repeat=reps, number=1)
print('qpixmap, python list:', min(res))

res = timeit.repeat(_qimage, repeat=reps, number=1)
print('qimage, python list:', min(res))

res = timeit.repeat(_bytearray, repeat=reps, number=1)
print('bytearray, python list:', min(res))


