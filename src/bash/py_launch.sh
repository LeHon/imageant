# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Utility code for launching python3 programs from the command line.
#


# The location of Python 3.  If no location is set, we assume that "python" or
# "python3" is in the user's PATH somewhere.
PY_PATH=


##
# Checks the path passed as a parameter to see if it is a valid path to a
# Python 3 executable.  Returns 0 if the path is valid, 1 otherwise.
##
checkPython3 () {
    # Check for an empty path.
    if [ -z "$1" ]; then
        return 1
    fi

    # Check if the path is interpretable as a command.
    if ! command -v "$1" > /dev/null; then
        return 1
    fi

    # Check the Python version.
    PY_VER=$("$(which $1)" -V 2>&1)
    if [[ "$PY_VER" == "Python 3."* ]]; then
        return 0
    fi

    return 1
}

##
# Launches a python script (and any additional command-line arguments).  The
# script path and any additional arguments should be provided as parameters.
##
launch () {
    # Check if we have a custom path for Python 3.
    checkPython3 "$PY_PATH"
    RETVAL=$?
    
    # If not, try to find Python 3.
    if [ $RETVAL -eq 1 ]; then
        PY_PATH="python"
        checkPython3 "$PY_PATH"
        RETVAL=$?
    
        if [ $RETVAL -eq 1 ]; then
            PY_PATH="python3"
            checkPython3 "$PY_PATH"
    	RETVAL=$?
        fi
    fi

    if [ $RETVAL -eq 1 ]; then
        printf "\nERROR: Python 3 appears to be missing.\n" >&2
        printf "Please install Python 3 in order to run this software.\n\n" >&2
        exit 1
    fi

    $PY_PATH "$@"
}

