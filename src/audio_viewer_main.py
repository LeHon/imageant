# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
from argparse import ArgumentParser
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon
from imageant.gui.audio_viewer import AudioViewer
from imageant.gui import ia_icons


argp = ArgumentParser(
    prog='audio_viewer', description='A simple audio file viewer and player.'
)
argp.add_argument(
    'audio_file', type=str, help='The path to an audio file.'
)

args = argp.parse_args()

app_name = 'ImageAnt Audio Viewer'

app = QApplication(sys.argv)
app.setOrganizationName(app_name)
app.setApplicationName(app_name)

QIcon.setThemeName('imageant')

audio_win = AudioViewer()

audio_win.loadMediaObject(args.audio_file)
audio_win.setWindowTitle(
    '{0} - {1}'.format(os.path.basename(args.audio_file), app_name)
)

audio_win.show()

sys.exit(app.exec_())

