# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
from argparse import ArgumentParser, Action
import math
import soundfile as sf
from imageant.audio.au_utils import ChoppedAudio


class ShowFormatsAction(Action):
    """
    An argparse Action class that prints all available audio formats and then
    exits the main program.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        f_dict = sf.available_formats()
        for format_str in sorted(f_dict.keys()):
            print('"{0}": {1}'.format(format_str, f_dict[format_str]))
        parser.exit(0)


argp = ArgumentParser(
    prog='audio_chopper', description='Chops an audio file into fixed-length '
    'chunks.'
)
argp.add_argument(
    '-l', '--length', type=float, required=False, default=10.0,
    help='The length of the output audio chunks, in seconds.'
)
argp.add_argument(
    '-k', '--keep_stereo', action='store_true', help='Keep both channels of '
    'stereo input files.  Otherwise, the output files will be converted to '
    'mono.'
)
argp.add_argument(
    '-o', '--output_folder', type=str, required=False, default='.',
    help='Location for the output files (default: ".").'
)
argp.add_argument(
    '-p', '--overlap', type=float, required=False, default=0.0,
    help='The overlap between successive audio chunks as a proportion of the '
    'chunk size (domain: [0.0-1.0), default: 0.0).'
)
argp.add_argument(
    '-m', '--input_max', type=float, required=False, default=None,
    help='The maximum length of input audio to process, in seconds (default: '
    'no limit).'
)
argp.add_argument(
    '-f', '--format', type=str, required=False, default='WAV',
    help='The format of the output audio file.  To see all available audio '
    'formats, run audio_chopper with the --show_formats option.  '
    'Default: "WAV".'
)
argp.add_argument(
    '--show_formats', nargs=0, action=ShowFormatsAction, help='Print '
    'information about all available output audio formats and exit.'
)
argp.add_argument(
    'audio_file', type=str, nargs='+', help='The path to an input audio file.'
)

args = argp.parse_args()

if not(os.path.isdir(args.output_folder)):
    if os.path.exists(args.output_folder):
        exit(
            '\nERROR: A file with the same name as the output folder already '
            'exists.\n'
        )
    else:
        os.mkdir(args.output_folder)

for audio_file in args.audio_file:
    if not(os.path.isfile(audio_file)):
        exit('\nERROR: The input audio file "{0}" could not be found.\n'.format(
            audio_file
        ))

    print('Processing file {0}...'.format(os.path.basename(audio_file)))

    try:
        ac = ChoppedAudio(
            audio_file, args.length, overlap=args.overlap,
            force_mono=not(args.keep_stereo), input_max=args.input_max
        )
    except Exception as err:
        exit('\nERROR: {0}\n'.format(err))

    max_digits = len(str(ac.getChunksInFile()))

    fout_parts = os.path.splitext(os.path.basename(audio_file))

    for chunk_cnt, s_chunk in enumerate(ac):
        fout_name = ('{0}-{1:0>{length}}.{2}').format(
            fout_parts[0], chunk_cnt + 1, args.format.lower(), length=max_digits
        )
        fout_path = os.path.join(args.output_folder, fout_name)
        if os.path.exists(fout_path):
            exit('\nERROR: The output file "{0}" already exists.\n'.format(
                fout_path
            ))
        print('  {0} (chunk {1} of {2})...'.format(
            fout_path, chunk_cnt + 1, len(ac)
        ))

        with sf.SoundFile(
            fout_path, mode='w', samplerate=ac.getSampleRate(),
            channels=ac.getOutputChannels(), format=args.format
        ) as fout:
            fout.write(s_chunk)

