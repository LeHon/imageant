# Overview

ImageAnt is desktop software for whole-image and audio annotation projects.  It supports complex annotation workflows via a built-in, easy-to-learn language for writing custom annotation scripts.  It runs on all major desktop operating systems, including GNU/Linux, Windows, and MacOS.


# Running ImageAnt

The easiest way to run ImageAnt is to download the self-contained ImageAnt executable for your target operating system.  The ImageAnt repository currently contains executables for Windows and MacOS:

  * [Windows executable](https://gitlab.com/stuckyb/imageant/raw/master/dist/windows/imageant.exe)
  * [MacOS executable](https://gitlab.com/stuckyb/imageant/raw/master/dist/mac/imageant.dmg)

To use either of these, you only need to download the executable for your operating system and run it.  There are no additional software dependencies or requirements to install.

To run ImageAnt on GNU/Linux, you currently need to run it directly from the source code.


# Creating ImageAnt annotation workflows

Annotation workflows are defined by writing annotation "scripts" using ImageAnt's custom annotation scripting language.  The scripting language is relatively simple and, I hope, easy to learn.  You can see an [example script](https://gitlab.com/stuckyb/imageant/blob/master/demo_script.ias) and check out [detailed documentation that describes the language and how to use it](https://gitlab.com/stuckyb/imageant/blob/master/doc/writing_task_scripts.md).

